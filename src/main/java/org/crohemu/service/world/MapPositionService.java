package org.crohemu.service.world;

import org.crohemu.model.world.GameMap;
import org.springframework.stereotype.Service;

@Service
public interface MapPositionService {

    GameMap findById(int id);
}
