package org.crohemu.service.world.path;

import org.crohemu.model.world.path.Direction;

import java.util.Arrays;
import java.util.List;

public class RolePlayPath {

    private final List<Short> keyMovements;

    public RolePlayPath(List<Short> keyMovements) {
        this.keyMovements = keyMovements;
    }

    public int getEndCellId() {
        return readCellId(keyMovements.get(keyMovements.size() - 1));
    }

    public Direction getEndDirection() {
        return readDirection(keyMovements.get(keyMovements.size() - 1));
    }

    private Direction readDirection(short keyMovement) {
        int directionValue = keyMovement >> 12;
        return Arrays.stream(Direction.values())
                .filter(direction -> direction.getValue() == directionValue)
                .findFirst().get();
    }

    private int readCellId(short keyMovement) {
        return keyMovement & 0b00001111_11111111;
    }
}
