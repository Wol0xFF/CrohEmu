package org.crohemu.service.world.impl;

import org.crohemu.model.world.GameMap;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayActorInformation;
import org.crohemu.persistence.world.map.GameMapRepository;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.common.ConversionService;
import org.crohemu.service.world.GameMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;


public class GameMapServiceImpl implements GameMapService {
    private final Logger logger = LoggerFactory.getLogger(GameMapServiceImpl.class);

    @Inject
    GameMapRepository gameMapRepository;

    @Inject
    ConversionService conversionService;

    @Inject
    CharacterService characterService;

    @Override
    public GameMap findById(int mapId) {
        GameMap gameMap = gameMapRepository.findById(mapId);
        if (gameMap == null) {
            logger.info("Unknown map with id {}", mapId);
        }
        return gameMap;
    }

    @Override
    public boolean mapIdExists(int mapId) {
        return this.gameMapRepository.findById(mapId) != null;
    }

    @Override
    public List<GameRolePlayActorInformation> getActors(GameMap gameMap) {

        List<GameRolePlayActorInformation> mapActors = new ArrayList<>();
        mapActors.addAll(this.characterService.findCharactersOnMap(gameMap.getId()).stream()
                .map(character -> this.conversionService.characterToGameRolePlayHumanoidInformation(character))
                .collect(Collectors.toSet()));

        // todo monsters etc

        return mapActors;
    }

    @Override
    public int getChangeMapCellId(int currentMapId, int targetMapId, int currentCellId) {
        ScrollType scrollType = getScrollType(currentMapId, targetMapId);
        switch (scrollType) {
            case TOP:
                return currentCellId + 532;
            case LEFT:
                return currentCellId + 27;
            case BOTTOM:
                return currentCellId - 532;
            case RIGHT:
                return currentCellId - 27;
            default:
                return 0; // todo get special cell id
        }
    }

    private ScrollType getScrollType(int startMapId, int targetMapId) {
        GameMap startMap = this.gameMapRepository.findById(startMapId);
        GameMap targetMap = this.gameMapRepository.findById(targetMapId);

        int xDiff = startMap.getPosX() - targetMap.getPosX();
        int yDiff = startMap.getPosY() - targetMap.getPosY();

        if (Math.abs(xDiff) > 1 || Math.abs(yDiff) > 1 ||
                (Math.abs(xDiff) == 1 && Math.abs(yDiff) == 1)) { // map are not next to each other
            return ScrollType.UNDEFINED;
        }

        if (xDiff == 1) {
            return ScrollType.LEFT;
        } else if (xDiff == -1) {
            return ScrollType.RIGHT;
        } else if (yDiff == 1) {
            return ScrollType.TOP;
        } else if (yDiff == -1) {
            return ScrollType.BOTTOM;
        }

        return ScrollType.UNDEFINED;
    }

    private enum ScrollType { UNDEFINED, TOP, RIGHT, BOTTOM, LEFT }
}
