package org.crohemu.service.world.impl;

import org.crohemu.model.world.GameMap;
import org.crohemu.persistence.world.MapPositionRepository;
import org.crohemu.service.world.MapPositionService;

import javax.inject.Inject;

public class MapPositionServiceImpl implements MapPositionService {

    @Inject
    private MapPositionRepository mapPositionRepository;

    @Override
    public GameMap findById(int id) {
        return mapPositionRepository.findById(id);
    }
}
