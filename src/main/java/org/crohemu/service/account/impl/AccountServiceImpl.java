package org.crohemu.service.account.impl;

import org.crohemu.model.account.Account;
import org.crohemu.persistence.account.AccountRepository;
import org.crohemu.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class AccountServiceImpl implements AccountService {

    private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Inject
    private AccountRepository accountRepository;

    @Override
    public IdentificationResult identify(String accountName, String password) {

        Account account = accountRepository.findByAccountNameAndPassword(accountName, password);

        if (account == null) {
            logger.info("Identification using invalid credentials {}, {}", accountName, password);
            return new IdentificationResult(IdentificationResult.Result.FAILURE_INVALID_CREDENTIALS, null, null);
        } else if (account.getBanEndDate().isAfterNow()) {
            logger.info("Identification failed because account {} is banned until {}",
                    account.getAccountId(), account.getBanEndDate());
            return new IdentificationResult(IdentificationResult.Result.FAILURE_BANNED, account.getBanEndDate(), null);
        } else {
            logger.info("Identification success for account {}", account.getAccountId());
            return new IdentificationResult(IdentificationResult.Result.SUCCESS, null, account);
        }
    }

    @Override
    public Account findByCharacterId(long characterId) {
        return this.accountRepository.findByCharactersCharacterId(characterId);
    }
}
