package org.crohemu.service.account.impl;

import org.apache.commons.lang3.StringUtils;
import org.crohemu.model.account.Account;
import org.crohemu.model.character.Breed;
import org.crohemu.model.character.Character;
import org.crohemu.model.character.CharacterBaseLook;
import org.crohemu.model.character.Inventory;
import org.crohemu.model.character.shortcut.Shortcuts;
import org.crohemu.network.d2protocol.message.messages.types.CharacterBaseInformation;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityLook;
import org.crohemu.persistence.account.AccountRepository;
import org.crohemu.persistence.character.BreedRepository;
import org.crohemu.persistence.character.HeadRepository;
import org.crohemu.server.game.GameServer;
import org.crohemu.server.game.config.GameServerConfig;
import org.crohemu.service.account.AccountService;
import org.crohemu.service.account.CharacterNameGenerator;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.account.CharacterService.CharacterMovementResult.Result;
import org.crohemu.service.common.ConversionService;
import org.crohemu.service.world.GameMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationResultMessage.ProtocolCharacterCreationResult.*;

public class CharacterServiceImpl implements CharacterService {

    public static final int START_MAP_ID = 0;
    public static final int START_CELL_ID = 100;
    public static final long CHARACTER_ID_LIMIT = 9007199254740990L;
    private final Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);

    @Inject
    GameServer gameServer;

    @Inject
    private BreedRepository breedRepository;

    @Inject
    private HeadRepository headRepository;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private CharacterNameGenerator characterNameGenerator;

    @Inject
    private GameMapService gameMapService;

    @Inject
    private ConversionService conversionService;

    @Inject
    private AccountService accountService;

    @Override
    public String generateName() {
        return characterNameGenerator.generate();
    }

    @Override
    public CharacterBaseInformation getCharacterBaseInformation(Character character) {

        CharacterBaseInformation characterBaseInformation = new CharacterBaseInformation();
        characterBaseInformation.setId(character.getCharacterId());
        characterBaseInformation.setBreed(character.getBreed());
        characterBaseInformation.setName(character.getName());
        characterBaseInformation.setLevel(character.getLevel());
        characterBaseInformation.setSex(character.getSex());

        EntityLook entityLook = new EntityLook();
        CharacterBaseLook characterBaseLook = character.getLook();
        entityLook.setBonesId(characterBaseLook.getBones().get(0));
        entityLook.setIndexedColors(conversionService.colorsToIndexedColors(characterBaseLook.getColors()));
        entityLook.setScales(characterBaseLook.getScales());
        entityLook.setSkins(characterBaseLook.getSkins());
        entityLook.setSubEntities(new ArrayList<>(0));

        characterBaseInformation.setEntityLook(entityLook);
        return characterBaseInformation;
    }

    @Override
    public ServiceCharacterCreationResult createCharacter(Account clientAccount, String name, int breedId,
                                                          boolean sex, List<Integer> colors, int cosmeticId) {

        int accountCharacterCount = clientAccount.getCharacters().size();
        if (accountCharacterCount >= GameServerConfig.getCharactersSlots()) {
            logger.info("Can't create a character for account {} (too many characters: {})",
                    clientAccount.getAccountId(), accountCharacterCount);
            return new ServiceCharacterCreationResult(FAILURE_TOO_MANY_CHARACTERS);
        } else if (this.isCharacterNameTaken(name)) {
          logger.info("Can't create a character for account {} (name already taken: {})",
                    clientAccount.getAccountId(), name);
            return new ServiceCharacterCreationResult(FAILURE_NAME_ALREADY_TAKEN);
        } else if (!this.isCharacterNameValid(name)) {
            logger.info("Can't create a character for account {} (invalid name: {})",
                    clientAccount.getAccountId(), name);
            return new ServiceCharacterCreationResult(FAILURE_INVALID_NAME);
        }

        long characterId = Math.abs(new Random().nextLong() % CHARACTER_ID_LIMIT);
        logger.info("Creating new character {} for account {}", characterId, clientAccount.getAccountId());

        Character newCharacter = new Character();
        Breed newCharacterBreed = breedRepository.findByBreedId(breedId);
        newCharacter.setSex(sex);
        newCharacter.setBreed(breedId);
        newCharacter.setCharacterId(characterId);
        newCharacter.setLevel(START_LEVEL);
        newCharacter.setName(name);
        CharacterBaseLook characterBaseLook = conversionService.entityLookToCharacterBaseLook(
                EntityLook.parse(newCharacterBreed.getLook(sex)));
        characterBaseLook.getSkins().add(headRepository.findById(cosmeticId).getSkins());
        characterBaseLook.setColors(colors);
        newCharacter.setLook(characterBaseLook);
        newCharacter.setCurrentMapId(START_MAP_ID);
        newCharacter.setCurrentCellId(START_CELL_ID);
        newCharacter.setInventory(new Inventory());
        newCharacter.setShortcuts(new Shortcuts());
        clientAccount.getCharacters().add(newCharacter);

        accountRepository.save(clientAccount);
        ServiceCharacterCreationResult result = new ServiceCharacterCreationResult(SUCCESS);
        result.newCharacter = newCharacter;
        return result;
    }

    @Override
    public void save(Character character) {
        Account account = this.accountRepository.findByCharactersCharacterId(character.getCharacterId());
        synchronized (character) {
            account.getCharacters().removeIf(c -> Objects.equals(c.getCharacterId(), character.getCharacterId()));
            account.getCharacters().add(character);
            this.accountRepository.save(account);
        }
    }

    @Override
    public void enterGame(Character character) {
        if (!gameMapService.mapIdExists(character.getCurrentMapId())) {
            this.moveCharacter(character, START_MAP_ID, START_CELL_ID);
        }
    }

    @Override
    public CharacterMovementResult moveCharacter(Character character, int mapId, int cellId) {
        CharacterMovementResult result = new CharacterMovementResult();
        result.oldMapId = mapId;
        result.oldCellId = cellId;

        // todo check that mapId and cellId are correct
        character.setCurrentMapId(mapId);
        character.setCurrentCellId(cellId);
        this.accountRepository.save(this.accountRepository.findByCharactersCharacterId(character.getCharacterId()));

        result.newMapId = mapId;
        result.newCellId = cellId;
        result.result = Result.SUCCESS;
        return result;
    }

    private boolean isCharacterNameTaken(String name) {
        return accountRepository.findByCharactersName(name) != null;
    }

    private boolean isCharacterNameValid(String name) {
        return (StringUtils.isNotBlank(name)
                && Pattern.compile("^[A-Z][a-z]{2,15}(?:-(?:[A-Z][a-z]{2,15}|[a-z]{3,15}))?$").matcher(name).matches())
                && name.length() >= 4 && name.length() <= 20;
    }

    @Override
    public CharacterDeletionResult deleteCharacter(Account clientAccount, long characterId, String secretAnswerHash) {
        List<Character> matchingCharacters = clientAccount.getCharacters()
                .stream()
                .filter(character -> character.getCharacterId() == characterId)
                .collect(Collectors.toList());

        if (matchingCharacters.size() == 0) {
            logger.info("Character deletion attempt failed because account {} does not own character {}",
                    clientAccount.getAccountId(), characterId);
            return CharacterDeletionResult.FAILURE_NO_CHARACTER_FOUND;
        } else if (matchingCharacters.size() > 1) {
            logger.info("Multiple characters with the same id found while deleting a character from account {}." +
                    " Deleting them all. characterId: {}", clientAccount.getAccountId(), characterId);
        }

        if (matchingCharacters.get(0).getLevel() >= MIN_LEVEL_DELETION_SECRET_ANSWER_REQUIRED) {
            try { // check secret answer
                byte[] expectedSecretAnswer = (clientAccount.getSecretAnswer() + "~" + characterId).getBytes("UTF-8");
                byte[] expectedHash = MessageDigest.getInstance("MD5").digest(expectedSecretAnswer);

                if (!Arrays.equals(expectedHash, secretAnswerHash.getBytes("UTF-8"))) {
                    logger.info("Wrong secret answer while deleting character {} for account {]",
                            characterId, clientAccount.getAccountId());
                    return CharacterDeletionResult.FAILURE_WRONG_SECRET_ANSWER;
                }
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        logger.info("Deleting character {} from account {}", characterId, clientAccount.getAccountId());
        clientAccount.getCharacters().removeAll(matchingCharacters);
        accountRepository.save(clientAccount);
        return CharacterDeletionResult.SUCCESS;
    }

    @Override
    public Set<Character> findCharactersOnMap(int mapId) {
        Set<Character> charactersInGame = gameServer.getCharactersInGame();
        return this.accountRepository.findByCharactersCurrentMapId(mapId).stream()
                .flatMap(account -> account.getCharacters().stream())
                .filter(character -> charactersInGame.stream()
                        .anyMatch(characterInGame ->
                                Objects.equals(character.getCharacterId(), characterInGame.getCharacterId())))
                .collect(Collectors.toSet());
    }

    @Override
    public void changeMap(Character character, int mapId) {
        int changeMapCellId = this.gameMapService.getChangeMapCellId(character.getCurrentMapId(), mapId,
                character.getCurrentCellId());
        character.setCurrentCellId(changeMapCellId);
        character.setCurrentMapId(mapId);
        this.save(character);
    }
}
