package org.crohemu.service.account;

@FunctionalInterface
public interface CharacterNameGenerator {
    String generate();
}
