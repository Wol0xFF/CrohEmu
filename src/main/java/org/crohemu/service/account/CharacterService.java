package org.crohemu.service.account;

import org.crohemu.model.account.Account;
import org.crohemu.model.character.Character;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationResultMessage.ProtocolCharacterCreationResult;
import org.crohemu.network.d2protocol.message.messages.types.CharacterBaseInformation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface CharacterService {
    int START_LEVEL = 1;
    int MIN_LEVEL_DELETION_SECRET_ANSWER_REQUIRED = 15;

    String generateName();

    CharacterBaseInformation getCharacterBaseInformation(Character character);

    ServiceCharacterCreationResult createCharacter(Account clientAccount, String name, int breedId,
                                                   boolean sex, List<Integer> colors, int cosmeticId);

    void save(Character character);

    Set<Character> findCharactersOnMap(int mapId);

    void changeMap(Character character, int mapId);

    class ServiceCharacterCreationResult {
        public ProtocolCharacterCreationResult resultType;
        public Character newCharacter;

        public ServiceCharacterCreationResult(ProtocolCharacterCreationResult resultType) {
            this.resultType = resultType;
        }
    }

    void enterGame(Character character);

    CharacterMovementResult moveCharacter(Character character, int mapId, int cellId);

    class CharacterMovementResult {
        public enum Result { SUCCESS, FAILURE }
        public Result result;
        public int oldMapId;
        public int oldCellId;
        public int newMapId;
        public int newCellId;
    }

    CharacterDeletionResult deleteCharacter(Account clientAccount, long characterId, String secretAnswerHash);
    enum CharacterDeletionResult {
        SUCCESS, FAILURE_WRONG_SECRET_ANSWER, FAILURE_NO_CHARACTER_FOUND

    }
}
