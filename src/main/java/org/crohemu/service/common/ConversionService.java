package org.crohemu.service.common;

import org.crohemu.model.character.Character;
import org.crohemu.model.character.CharacterBaseLook;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayHumanoidInformation;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityLook;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ConversionService {
    CharacterBaseLook entityLookToCharacterBaseLook(EntityLook entityLook);

    List<Integer> indexedColorsToColors(List<Integer> indexedColors);

    List<Integer> colorsToIndexedColors(List<Integer> colors);

    EntityLook characterBaseLookToEntityLook(CharacterBaseLook characterBaseLook);

    GameRolePlayHumanoidInformation characterToGameRolePlayHumanoidInformation(Character character);
}
