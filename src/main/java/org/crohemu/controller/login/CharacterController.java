package org.crohemu.controller.login;

import org.crohemu.ServerConsole;
import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.model.account.Account;
import org.crohemu.model.character.Character;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.emote.EmoteListMessage;
import org.crohemu.network.d2protocol.message.messages.game.inventory.InventoryContentMessage;
import org.crohemu.network.d2protocol.message.messages.game.inventory.InventoryWeightMessage;
import org.crohemu.network.d2protocol.message.messages.game.shortcut.ShortcutBarContentMessage;
import org.crohemu.network.d2protocol.message.messages.login.CharacterLoadingCompleteMessage;
import org.crohemu.network.d2protocol.message.messages.login.CharacterSelectedErrorMessage;
import org.crohemu.network.d2protocol.message.messages.login.CharacterSelectedSuccessMessage;
import org.crohemu.network.d2protocol.message.messages.login.CharacterSelectionMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharacterDeletionErrorMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharacterDeletionRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharactersListMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharactersListRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationResultMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationResultMessage.ProtocolCharacterCreationResult;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterNameSuggestionRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterNameSuggestionSuccessMessage;
import org.crohemu.network.d2protocol.message.messages.types.CharacterBaseInformation;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.persistence.character.EmoticonRepository;
import org.crohemu.server.game.GameServer;
import org.crohemu.service.account.AccountService;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.account.CharacterService.CharacterDeletionResult;
import org.crohemu.service.account.CharacterService.ServiceCharacterCreationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.crohemu.network.d2protocol.message.messages.game.shortcut.ShortcutBarContentMessage.TYPE_GENERAL_SHORTCUT_BAR;
import static org.crohemu.network.d2protocol.message.messages.game.shortcut.ShortcutBarContentMessage.TYPE_SPELL_SHORTCUT_BAR;

@D2Controller(serverType = D2Server.Type.GAME)
public class CharacterController {
    private Logger logger = LoggerFactory.getLogger(CharacterController.class);

    @Inject
    ServerConsole serverConsole;

    @Inject
    GameServer gameServer;

    @Inject
    AccountService accountService;

    @Inject
    CharacterService characterService;

    @Inject
    EmoticonRepository emoticonRepository;

    @D2MessageReceiver
    public void requestCharactersList(CharactersListRequestMessage message, D2Client source) {
        if (!source.isAuthenticated()) {
            logger.error("Client {} requested characters list but is not authenticated", source);
            gameServer.disconnectClient(source);
            return;
        }
        source.sendMessage(buildCharactersListMessage(source.getAccount()));
    }

    @D2MessageReceiver
    public void suggestCharacterName(CharacterNameSuggestionRequestMessage message, D2Client source) {
        source.sendMessage(new CharacterNameSuggestionSuccessMessage(characterService.generateName()));
    }

    @D2MessageReceiver
    public void selectCharacter(CharacterSelectionMessage message, D2Client source) {
        if (!source.isAuthenticated()) {
            logger.error("Client {} tried to select a character but is not authenticated!", source);
            gameServer.disconnectClient(source);
            return;
        }

        Optional<Character> accountCharacter = source.getAccount().getCharacters().stream()
                .filter(character -> character.getCharacterId() == message.getId())
                .findFirst();
        if (!accountCharacter.isPresent()) {
            logger.error("Client {} tried to select character {} but is not registered to his account ({})",
                    source, source.getAccount().getAccountId());
            source.sendMessage(new CharacterSelectedErrorMessage());
        } else {
            Character character = accountCharacter.get();
            logger.info("Client {} selected character {} successfully", source, character.getCharacterId());
            this.connectCharacter(character, source);
        }
    }

    @D2MessageReceiver
    public void requestCharacterCreation(CharacterCreationRequestMessage message, D2Client source) {
        if (!source.isAuthenticated()) {
            logger.error("Client {} requested the creation of a character but is not authenticated !", source);
            gameServer.disconnectClient(source);
            return;
        }

        ServiceCharacterCreationResult characterCreationResult = characterService.createCharacter(
                source.getAccount(), message.getName(), message.getBreed(), message.getSex(),
                message.getColors(), message.getCosmeticId());

        source.sendMessage(new CharacterCreationResultMessage(characterCreationResult.resultType));
        if (characterCreationResult.resultType == ProtocolCharacterCreationResult.SUCCESS) {
            this.connectCharacter(characterCreationResult.newCharacter, source);
        }
    }

    private void connectCharacter(Character character, D2Client source) {

        source.setCurrentCharacter(character);
        this.characterService.enterGame(character);

        source.sendMessage(new CharacterSelectedSuccessMessage(characterService.getCharacterBaseInformation(character)));

        source.sendMessage(new InventoryContentMessage(character.getInventory().getItems(), character.getInventory().getKamas()));
        source.sendMessage(new InventoryWeightMessage(character.getInventory().getWeight(), character.getMaxWeight()));

        source.sendMessage(new ShortcutBarContentMessage(TYPE_GENERAL_SHORTCUT_BAR, character.getShortcuts().getGeneralShortcutBar()));
        source.sendMessage(new ShortcutBarContentMessage(TYPE_SPELL_SHORTCUT_BAR, character.getShortcuts().getSpellShortcutBar()));

        source.sendMessage(new EmoteListMessage(emoticonRepository.findAllIds()));

        // todo load everything
        source.sendMessage(new CharacterLoadingCompleteMessage());
    }

    @D2MessageReceiver
    public void requestCharacterDeletion(CharacterDeletionRequestMessage message, D2Client source) {
        Account clientAccount = source.getAccount();
        CharacterDeletionResult characterDeletionResult = characterService.deleteCharacter(
                clientAccount, message.getCharacterId(), message.getSecretAnswerHash());

        if (characterDeletionResult == CharacterDeletionResult.FAILURE_WRONG_SECRET_ANSWER) {
            source.sendMessage(new CharacterDeletionErrorMessage(CharacterDeletionErrorMessage.REASON_WRONG_SECRET_ANSWER));
        } else if (characterDeletionResult == CharacterDeletionResult.SUCCESS) {
            source.sendMessage(this.buildCharactersListMessage(clientAccount));
        } else {
            source.sendMessage(new CharacterDeletionErrorMessage(CharacterDeletionErrorMessage.REASON_NO_REASON));
        }
    }

    private CharactersListMessage buildCharactersListMessage(Account clientAccount) {
        List<CharacterBaseInformation> characterBaseInformationList = clientAccount.getCharacters()
                .stream()
                .map(character -> characterService.getCharacterBaseInformation(character))
                .collect(Collectors.toList());

        CharactersListMessage charactersListMessage = new CharactersListMessage();
        charactersListMessage.setCharacters(characterBaseInformationList);
        charactersListMessage.setHasStartupActions(false);
        return charactersListMessage;
    }
}
