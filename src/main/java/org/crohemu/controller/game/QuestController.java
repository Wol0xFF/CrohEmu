package org.crohemu.controller.game;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.quest.QuestListMessage;
import org.crohemu.network.d2protocol.message.messages.game.quest.QuestListRequestMessage;
import org.crohemu.network.d2protocol.server.D2Server;

import java.util.ArrayList;

@D2Controller(serverType = D2Server.Type.GAME)
public class QuestController {

    @D2MessageReceiver
    public void requestQuestList(QuestListRequestMessage message, D2Client source) {
        // todo
        QuestListMessage questListMessage = new QuestListMessage();
        questListMessage.setActiveQuests(new ArrayList<>());
        questListMessage.setFinishedQuestsCounts(new ArrayList<>());
        questListMessage.setFinishedQuestsIds(new ArrayList<>());
        questListMessage.setReinitDoneQuestsIds(new ArrayList<>());
        source.sendMessage(questListMessage);
    }
}
