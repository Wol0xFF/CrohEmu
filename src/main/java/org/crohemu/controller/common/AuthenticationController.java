package org.crohemu.controller.common;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.factory.D2MessageFactory;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.auth.ClientKeyMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicAckMessage;
import org.crohemu.network.d2protocol.message.messages.game.auth.AuthenticationTicketAcceptedMessage;
import org.crohemu.network.d2protocol.message.messages.game.auth.AuthenticationTicketMessage;
import org.crohemu.network.d2protocol.message.messages.game.auth.AuthenticationTicketRefusedMessage;
import org.crohemu.network.d2protocol.message.messages.game.auth.TrustStatusMessage;
import org.crohemu.network.d2protocol.message.messages.login.ReloginTokenRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.ReloginTokenStatusMessage;
import org.crohemu.network.d2protocol.message.messages.misc.BasicTimeMessage;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.server.game.GameServer;
import org.crohemu.server.login.LoginServer;
import org.joda.time.Instant;

import javax.inject.Inject;

@D2Controller(serverType = D2Server.Type.BOTH)
public class AuthenticationController {
    @Inject
    LoginServer loginServer;

    @Inject
    GameServer gameServer;

    @Inject
    D2MessageFactory d2MessageFactory;

    @D2MessageReceiver
    public void authenticate(AuthenticationTicketMessage message, D2Client source) {
        if (!source.isAuthenticated() && !gameServer.authenticateClient(source, message.getTicket())) {
            source.sendMessage(new AuthenticationTicketRefusedMessage());
            return;
        }

        source.sendMessage(new BasicAckMessage(0, D2MessageType.AUTHENTICATION_TICKET.getId()));
        source.sendMessage(new AuthenticationTicketAcceptedMessage());
        source.sendMessage(new BasicTimeMessage(new Instant().getMillis(), 0));
        source.sendMessage(d2MessageFactory.buildServerSettingsMessage());
        source.sendMessage(d2MessageFactory.buildAccountCapabilitiesMessage(source.getAccount()));
        source.sendMessage(new TrustStatusMessage(true, true));
    }

    @D2MessageReceiver
    public void requestReloginToken(ReloginTokenRequestMessage message, D2Client source) {
        // this is a placeholder. todo; need to patch the client again.

        ReloginTokenStatusMessage reloginTokenStatusMessage = new ReloginTokenStatusMessage();
        reloginTokenStatusMessage.setValidToken(false);
        reloginTokenStatusMessage.setTicket(new int[0]);

        source.sendMessage(reloginTokenStatusMessage);
    }

    @D2MessageReceiver
    public void sendClientKey(ClientKeyMessage message, D2Client source) {
        // todo?
    }
}
