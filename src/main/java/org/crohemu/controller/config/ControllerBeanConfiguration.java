package org.crohemu.controller.config;

import org.crohemu.ApplicationContextProvider;
import org.crohemu.admin.AdminCommandFactory;
import org.crohemu.controller.admin.AdminCommandController;
import org.crohemu.controller.common.AuthenticationController;
import org.crohemu.controller.common.MiscellaneousController;
import org.crohemu.controller.game.GameContextController;
import org.crohemu.controller.game.MapController;
import org.crohemu.controller.game.QuestController;
import org.crohemu.controller.game.SocialController;
import org.crohemu.controller.login.CharacterController;
import org.crohemu.controller.login.LoginController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllerBeanConfiguration {

    @Bean
    ApplicationContextProvider applicationContextProvider() { return new ApplicationContextProvider(); }

    @Bean
    LoginController loginController() {
        return new LoginController();
    }

    @Bean
    MiscellaneousController miscellaneousController() { return new MiscellaneousController(); }

    @Bean
    AuthenticationController authenticationController() { return new AuthenticationController(); }

    @Bean
    CharacterController characterSelectionController() { return new CharacterController(); }

    @Bean
    SocialController socialController() { return new SocialController(); }

    @Bean
    GameContextController gameContextController() { return new GameContextController(); }

    @Bean
    QuestController questController() { return new QuestController(); }

    @Bean
    MapController mapController() { return new MapController(); }

    @Bean
    AdminCommandController adminCommandController() { return new AdminCommandController(); }

    @Bean
    AdminCommandFactory adminCommandFactory() { return new AdminCommandFactory(); }
}
