package org.crohemu.model.character;

import org.crohemu.model.character.inventory.InventoryItem;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private int kamas = 0;

    private List<InventoryItem> items = new ArrayList<>();

    public int getKamas() {
        return kamas;
    }

    public void setKamas(int kamas) {
        this.kamas = kamas;
    }

    public List<InventoryItem> getItems() {
        return items;
    }

    public void setItems(List<InventoryItem> items) {
        this.items = items;
    }

    public int getWeight() {
        return 0; // todo
    }
}
