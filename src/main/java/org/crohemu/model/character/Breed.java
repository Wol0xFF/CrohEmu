package org.crohemu.model.character;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

import static org.crohemu.model.character.Character.SEX_FEMALE;

@Document(collection = "Breed")
public class Breed {
    @Id
    private ObjectId _id;

    /**
     * functional id
     */
    private int breedId;
    private int shortNameId;
    private int longNameId;
    private int descriptionId;
    private int gameplayDescriptionId;
    private String maleLook;
    private String femaleLook;
    private int creatureBonesId;
    private int maleArtwork;
    private int femaleArtwork;
    private List<List<Integer>> statsPointsForStrength;
    private List<List<Integer>> statsPointsForIntelligence;
    private List<List<Integer>> statsPointsForChance;
    private List<List<Integer>> statsPointsForAgility;
    private List<List<Integer>> statsPointsForVitality;
    private List<List<Integer>> statsPointsForWisdom;
    private List<Integer> breedSpellsId;
    private List<BreedRole> breedRoles;
    private List<Integer> maleColors;
    private List<Integer> femaleColors;
    private int spawnMap;
    private int complexity;
    private int sortIndex;

    public int getBreedId() {
        return breedId;
    }

    public void setBreedId(int breedId) {
        this.breedId = breedId;
    }

    public int getShortNameId() {
        return shortNameId;
    }

    public void setShortNameId(int shortNameId) {
        this.shortNameId = shortNameId;
    }

    public int getLongNameId() {
        return longNameId;
    }

    public void setLongNameId(int longNameId) {
        this.longNameId = longNameId;
    }

    public int getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(int descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getGameplayDescriptionId() {
        return gameplayDescriptionId;
    }

    public void setGameplayDescriptionId(int gameplayDescriptionId) {
        this.gameplayDescriptionId = gameplayDescriptionId;
    }

    public String getMaleLook() {
        return maleLook;
    }

    public void setMaleLook(String maleLook) {
        this.maleLook = maleLook;
    }

    public String getFemaleLook() {
        return femaleLook;
    }

    public void setFemaleLook(String femaleLook) {
        this.femaleLook = femaleLook;
    }

    public int getCreatureBonesId() {
        return creatureBonesId;
    }

    public void setCreatureBonesId(int creatureBonesId) {
        this.creatureBonesId = creatureBonesId;
    }

    public int getMaleArtwork() {
        return maleArtwork;
    }

    public void setMaleArtwork(int maleArtwork) {
        this.maleArtwork = maleArtwork;
    }

    public int getFemaleArtwork() {
        return femaleArtwork;
    }

    public void setFemaleArtwork(int femaleArtwork) {
        this.femaleArtwork = femaleArtwork;
    }

    public List<List<Integer>> getStatsPointsForStrength() {
        return statsPointsForStrength;
    }

    public void setStatsPointsForStrength(List<List<Integer>> statsPointsForStrength) {
        this.statsPointsForStrength = statsPointsForStrength;
    }

    public List<List<Integer>> getStatsPointsForIntelligence() {
        return statsPointsForIntelligence;
    }

    public void setStatsPointsForIntelligence(List<List<Integer>> statsPointsForIntelligence) {
        this.statsPointsForIntelligence = statsPointsForIntelligence;
    }

    public List<List<Integer>> getStatsPointsForChance() {
        return statsPointsForChance;
    }

    public void setStatsPointsForChance(List<List<Integer>> statsPointsForChance) {
        this.statsPointsForChance = statsPointsForChance;
    }

    public List<List<Integer>> getStatsPointsForAgility() {
        return statsPointsForAgility;
    }

    public void setStatsPointsForAgility(List<List<Integer>> statsPointsForAgility) {
        this.statsPointsForAgility = statsPointsForAgility;
    }

    public List<List<Integer>> getStatsPointsForVitality() {
        return statsPointsForVitality;
    }

    public void setStatsPointsForVitality(List<List<Integer>> statsPointsForVitality) {
        this.statsPointsForVitality = statsPointsForVitality;
    }

    public List<List<Integer>> getStatsPointsForWisdom() {
        return statsPointsForWisdom;
    }

    public void setStatsPointsForWisdom(List<List<Integer>> statsPointsForWisdom) {
        this.statsPointsForWisdom = statsPointsForWisdom;
    }

    public List<Integer> getBreedSpellsId() {
        return breedSpellsId;
    }

    public void setBreedSpellsId(List<Integer> breedSpellsId) {
        this.breedSpellsId = breedSpellsId;
    }

    public List<BreedRole> getBreedRoles() {
        return breedRoles;
    }

    public void setBreedRoles(List<BreedRole> breedRoles) {
        this.breedRoles = breedRoles;
    }

    public List<Integer> getMaleColors() {
        return maleColors;
    }

    public void setMaleColors(List<Integer> maleColors) {
        this.maleColors = maleColors;
    }

    public List<Integer> getFemaleColors() {
        return femaleColors;
    }

    public void setFemaleColors(List<Integer> femaleColors) {
        this.femaleColors = femaleColors;
    }

    public int getSpawnMap() {
        return spawnMap;
    }

    public void setSpawnMap(int spawnMap) {
        this.spawnMap = spawnMap;
    }

    public int getComplexity() {
        return complexity;
    }

    public void setComplexity(int complexity) {
        this.complexity = complexity;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getLook(boolean sex) {
        return sex == SEX_FEMALE ? femaleLook : maleLook;
    }
}
