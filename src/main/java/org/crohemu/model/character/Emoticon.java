package org.crohemu.model.character;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Emoticon")
public class Emoticon {

    @Id
    String _id;

    private int id;
    private int nameId;
    private int shortcutId;
    private int order;
    private String defaultAnim;
    private boolean persistancy;
    private boolean eight_directions;
    private boolean aura;
    private List<String> anims;
    private int cooldown;
    private int duration;
    private int weight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public int getShortcutId() {
        return shortcutId;
    }

    public void setShortcutId(int shortcutId) {
        this.shortcutId = shortcutId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getDefaultAnim() {
        return defaultAnim;
    }

    public void setDefaultAnim(String defaultAnim) {
        this.defaultAnim = defaultAnim;
    }

    public boolean isPersistancy() {
        return persistancy;
    }

    public void setPersistancy(boolean persistancy) {
        this.persistancy = persistancy;
    }

    public boolean isEight_directions() {
        return eight_directions;
    }

    public void setEight_directions(boolean eight_directions) {
        this.eight_directions = eight_directions;
    }

    public boolean isAura() {
        return aura;
    }

    public void setAura(boolean aura) {
        this.aura = aura;
    }

    public List<String> getAnims() {
        return anims;
    }

    public void setAnims(List<String> anims) {
        this.anims = anims;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
