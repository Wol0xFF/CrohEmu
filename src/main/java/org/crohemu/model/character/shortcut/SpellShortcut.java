package org.crohemu.model.character.shortcut;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;

import java.io.IOException;

public class SpellShortcut extends Shortcut {

    protected int spellId;

    public SpellShortcut() {
        typeId = 368;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            super.serialize(outputStream);
            outputStream.writeVariableLengthShort(spellId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        super.deserialize(inputStream);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && spellId >= 0;
    }

    public int getSpellId() {
        return spellId;
    }

    public void setSpellId(int spellId) {
        this.spellId = spellId;
    }
}
