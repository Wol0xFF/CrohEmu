package org.crohemu.model.character.shortcut;

import java.util.ArrayList;
import java.util.List;

public class Shortcuts {

    private List<Shortcut> generalShortcutBar = new ArrayList<>();
    private List<SpellShortcut> spellShortcutBar = new ArrayList<>();

    public List<Shortcut> getGeneralShortcutBar() {
        return generalShortcutBar;
    }

    public void setGeneralShortcutBar(List<Shortcut> generalShortcutBar) {
        this.generalShortcutBar = generalShortcutBar;
    }

    public List<SpellShortcut> getSpellShortcutBar() {
        return spellShortcutBar;
    }

    public void setSpellShortcutBar(List<SpellShortcut> spellShortcutBar) {
        this.spellShortcutBar = spellShortcutBar;
    }
}
