package org.crohemu.model.item;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Item")
public class Item {

    @Id
    private String _id;

    private int id;

    private int nameId;

    private int typeId;

    private int descriptionId;

    private int iconId;

    private int level;

    private int realWeight;

    private boolean cursed;

    private int useAnimationId;

    private boolean usable;

    private boolean targetable;

    private boolean exchangeable;

    private long price;

    private boolean twoHanded;

    private boolean etheral;

    private int itemSetId;

    private String criteria;

    private String criteriaTarget;

    private boolean hideEffects;

    private boolean enhanceable;

    private boolean nonUsableOnAnother;

    private int appearanceId;

    private boolean secretRecipe;

    private List<Integer> dropMonsterIds;

    private int recipeSlots;

    private List<Integer> recipeIds;

    private boolean bonusIsSecret;

    private List<EffectInstance> possibleEffects;

    private List<Integer> favoriteSubAreas;

    private int favoriteSubAreasBonus;

    private int craftXpRatio;

    private boolean needUseConfirm;

    private boolean isDestructible;

    private List<List<Long>> nuggetsBySubarea;

    private List<Integer> containerIds;

    private List<List<Integer>> resourcesBySubarea;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(int descriptionId) {
        this.descriptionId = descriptionId;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRealWeight() {
        return realWeight;
    }

    public void setRealWeight(int realWeight) {
        this.realWeight = realWeight;
    }

    public boolean isCursed() {
        return cursed;
    }

    public void setCursed(boolean cursed) {
        this.cursed = cursed;
    }

    public int getUseAnimationId() {
        return useAnimationId;
    }

    public void setUseAnimationId(int useAnimationId) {
        this.useAnimationId = useAnimationId;
    }

    public boolean isUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }

    public boolean isTargetable() {
        return targetable;
    }

    public void setTargetable(boolean targetable) {
        this.targetable = targetable;
    }

    public boolean isExchangeable() {
        return exchangeable;
    }

    public void setExchangeable(boolean exchangeable) {
        this.exchangeable = exchangeable;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public boolean isTwoHanded() {
        return twoHanded;
    }

    public void setTwoHanded(boolean twoHanded) {
        this.twoHanded = twoHanded;
    }

    public boolean isEtheral() {
        return etheral;
    }

    public void setEtheral(boolean etheral) {
        this.etheral = etheral;
    }

    public int getItemSetId() {
        return itemSetId;
    }

    public void setItemSetId(int itemSetId) {
        this.itemSetId = itemSetId;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getCriteriaTarget() {
        return criteriaTarget;
    }

    public void setCriteriaTarget(String criteriaTarget) {
        this.criteriaTarget = criteriaTarget;
    }

    public boolean isHideEffects() {
        return hideEffects;
    }

    public void setHideEffects(boolean hideEffects) {
        this.hideEffects = hideEffects;
    }

    public boolean isEnhanceable() {
        return enhanceable;
    }

    public void setEnhanceable(boolean enhanceable) {
        this.enhanceable = enhanceable;
    }

    public boolean isNonUsableOnAnother() {
        return nonUsableOnAnother;
    }

    public void setNonUsableOnAnother(boolean nonUsableOnAnother) {
        this.nonUsableOnAnother = nonUsableOnAnother;
    }

    public int getAppearanceId() {
        return appearanceId;
    }

    public void setAppearanceId(int appearanceId) {
        this.appearanceId = appearanceId;
    }

    public boolean isSecretRecipe() {
        return secretRecipe;
    }

    public void setSecretRecipe(boolean secretRecipe) {
        this.secretRecipe = secretRecipe;
    }

    public List<Integer> getDropMonsterIds() {
        return dropMonsterIds;
    }

    public void setDropMonsterIds(List<Integer> dropMonsterIds) {
        this.dropMonsterIds = dropMonsterIds;
    }

    public int getRecipeSlots() {
        return recipeSlots;
    }

    public void setRecipeSlots(int recipeSlots) {
        this.recipeSlots = recipeSlots;
    }

    public List<Integer> getRecipeIds() {
        return recipeIds;
    }

    public void setRecipeIds(List<Integer> recipeIds) {
        this.recipeIds = recipeIds;
    }

    public boolean isBonusIsSecret() {
        return bonusIsSecret;
    }

    public void setBonusIsSecret(boolean bonusIsSecret) {
        this.bonusIsSecret = bonusIsSecret;
    }

    public List<EffectInstance> getPossibleEffects() {
        return possibleEffects;
    }

    public void setPossibleEffects(List<EffectInstance> possibleEffects) {
        this.possibleEffects = possibleEffects;
    }

    public List<Integer> getFavoriteSubAreas() {
        return favoriteSubAreas;
    }

    public void setFavoriteSubAreas(List<Integer> favoriteSubAreas) {
        this.favoriteSubAreas = favoriteSubAreas;
    }

    public int getFavoriteSubAreasBonus() {
        return favoriteSubAreasBonus;
    }

    public void setFavoriteSubAreasBonus(int favoriteSubAreasBonus) {
        this.favoriteSubAreasBonus = favoriteSubAreasBonus;
    }

    public int getCraftXpRatio() {
        return craftXpRatio;
    }

    public void setCraftXpRatio(int craftXpRatio) {
        this.craftXpRatio = craftXpRatio;
    }

    public boolean isNeedUseConfirm() {
        return needUseConfirm;
    }

    public void setNeedUseConfirm(boolean needUseConfirm) {
        this.needUseConfirm = needUseConfirm;
    }

    public boolean isDestructible() {
        return isDestructible;
    }

    public void setDestructible(boolean destructible) {
        isDestructible = destructible;
    }

    public List<List<Long>> getNuggetsBySubarea() {
        return nuggetsBySubarea;
    }

    public void setNuggetsBySubarea(List<List<Long>> nuggetsBySubarea) {
        this.nuggetsBySubarea = nuggetsBySubarea;
    }

    public List<Integer> getContainerIds() {
        return containerIds;
    }

    public void setContainerIds(List<Integer> containerIds) {
        this.containerIds = containerIds;
    }

    public List<List<Integer>> getResourcesBySubarea() {
        return resourcesBySubarea;
    }

    public void setResourcesBySubarea(List<List<Integer>> resourcesBySubarea) {
        this.resourcesBySubarea = resourcesBySubarea;
    }
}
