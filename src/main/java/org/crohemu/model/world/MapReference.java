package org.crohemu.model.world;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MapReference")
public class MapReference {
    private int id;
    private int mapId;
    private int cellId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }
}
