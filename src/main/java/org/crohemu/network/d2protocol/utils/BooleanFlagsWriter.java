package org.crohemu.network.d2protocol.utils;

/**
 * Writes bits in a byte as flags.
 */
public class BooleanFlagsWriter {

    /**
     * The byte that contains the flags values.
     */
    private byte flags;

    /**
     * Creates a BooleanFlagsWriter with all flags to false.
     */
    public BooleanFlagsWriter() {
        this((byte) 0);
    }

    /**
     * Creates a BooleanFlagsWriter from an existing byte value.
     * @param flags
     */
    public BooleanFlagsWriter(byte flags) {
        this.flags = flags;
    }

    /**
     * Writes flag <i>value</i> at bit <i>position</i>.
     * position 0 is the LSB, position 7 is the MSB.
     * @param position
     * @param value
     */
    public void writeFlag(int position, boolean value) {
        int mask = (value ? 1 : 0) << position;
        flags = (byte) (flags | mask);
    }

    /**
     * @return The byte that contains the flags.
     */
    public byte flagsToByte() {
        return flags;
    }

    /**
     * Returns a byte directly from an array of booleans.
     * @param flags The flags. Maximum is 8 flags.
     * @return A byte containing the flags.
     */
    public static byte flagsToByte(boolean... flags) {
        if (flags.length > Byte.SIZE) {
            throw new IllegalArgumentException("Too many flags. Maximum is " + Byte.SIZE);
        }

        BooleanFlagsWriter writer = new BooleanFlagsWriter();

        for (int i = 0; i < flags.length; i++) {
            writer.writeFlag(i, flags[i]);
        }

        return writer.flagsToByte();
    }
}
