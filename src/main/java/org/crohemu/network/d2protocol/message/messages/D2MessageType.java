package org.crohemu.network.d2protocol.message.messages;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.auth.ClientKeyMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicAckMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicPingMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicPongMessage;
import org.crohemu.network.d2protocol.message.messages.common.ProtocolRequiredMessage;
import org.crohemu.network.d2protocol.message.messages.discarded.HaapiApiKeyRequestMessage;
import org.crohemu.network.d2protocol.message.messages.game.GameContextCreateRequestMessage;
import org.crohemu.network.d2protocol.message.messages.game.HelloGameMessage;
import org.crohemu.network.d2protocol.message.messages.game.ServerOptionalFeaturesMessage;
import org.crohemu.network.d2protocol.message.messages.game.auth.*;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextCreateMessage;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextDestroyMessage;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextRemoveElementMessage;
import org.crohemu.network.d2protocol.message.messages.game.emote.EmoteListMessage;
import org.crohemu.network.d2protocol.message.messages.game.inventory.InventoryContentMessage;
import org.crohemu.network.d2protocol.message.messages.game.inventory.InventoryWeightMessage;
import org.crohemu.network.d2protocol.message.messages.game.map.*;
import org.crohemu.network.d2protocol.message.messages.game.quest.QuestListMessage;
import org.crohemu.network.d2protocol.message.messages.game.quest.QuestListRequestMessage;
import org.crohemu.network.d2protocol.message.messages.game.shortcut.ShortcutBarContentMessage;
import org.crohemu.network.d2protocol.message.messages.game.social.*;
import org.crohemu.network.d2protocol.message.messages.login.*;
import org.crohemu.network.d2protocol.message.messages.login.character.CharacterDeletionErrorMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharacterDeletionRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharactersListMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.CharactersListRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterCreationResultMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterNameSuggestionRequestMessage;
import org.crohemu.network.d2protocol.message.messages.login.character.creation.CharacterNameSuggestionSuccessMessage;
import org.crohemu.network.d2protocol.message.messages.misc.BasicTimeMessage;

/**
 * Each and every type of message that can be sent or received by the server.
 */
public enum D2MessageType {
    PROTOCOL_REQUIRED(1, ProtocolRequiredMessage.class),
    HELLO_CONNECT(3, HelloConnectMessage.class),
    IDENTIFICATION(4, IdentificationMessage.class),
    IDENTIFICATION_FAILED(20, IdentificationFailedMessage.class),
    IDENTIFICATION_SUCCESS(22, IdentificationSuccessMessage.class),
    SERVERS_LIST(30, ServersListMessage.class),
    SERVER_SELECTION(40, ServerSelectionMessage.class),
    SELECTED_SERVER_DATA(42, SelectedServerDataMessage.class),
    HELLO_GAME(101, HelloGameMessage.class),
    AUTHENTICATION_TICKET(110, AuthenticationTicketMessage.class),
    AUTHENTICATION_TICKET_ACCEPTED(111, AuthenticationTicketAcceptedMessage.class),
    AUTHENTICATION_TICKET_REFUSED(112, AuthenticationTicketRefusedMessage.class),
    CHARACTERS_LIST_REQUEST(150, CharactersListRequestMessage.class),
    CHARACTERS_LIST(151, CharactersListMessage.class),
    CHARACTER_SELECTION(152, CharacterSelectionMessage.class),
    CHARACTER_SELECTED_SUCCESS(153, CharacterSelectedSuccessMessage.class),
    CHARACTER_CREATION_REQUEST(160, CharacterCreationRequestMessage.class),
    CHARACTER_CREATION_RESULT(161, CharacterCreationResultMessage.class),
    CHARACTER_NAME_SUGGESTION_REQUEST(162, CharacterNameSuggestionRequestMessage.class),
    CHARACTER_DELETION_REQUEST(165, CharacterDeletionRequestMessage.class),
    CHARACTER_DELETION_ERROR(166, CharacterDeletionErrorMessage.class),
    BASIC_TIME(175, BasicTimeMessage.class),
    BASIC_PING(182, BasicPingMessage.class),
    BASIC_PONG(183, BasicPongMessage.class),
    GAME_CONTEXT_CREATE(200, GameContextCreateMessage.class),
    GAME_CONTEXT_DESTROY(201, GameContextDestroyMessage.class),
    CURRENT_MAP(220, CurrentMapMessage.class),
    CHANGE_MAP(221, ChangeMapMessage.class),
    MAP_INFORMATION_REQUEST(225, MapInformationRequestMessage.class),
    MAP_COMPLEMENTARY_INFORMATION_DATA(226, MapComplementaryInformationDataMessage.class),
    GAME_CONTEXT_CREATE_REQUEST(250, GameContextCreateRequestMessage.class),
    GAME_CONTEXT_REMOVE_ELEMENT(251, GameContextRemoveElementMessage.class),
    GAME_MAP_MOVEMENT_REQUEST(950, GameMapMovementRequestMessage.class),
    GAME_MAP_MOVEMENT(951, GameMapMovementMessage.class),
    GAME_MAP_MOVEMENT_CONFIRM(952, GameMapMovementConfirmMessage.class),
    GAME_MAP_MOVEMENT_CANCEL(953, GameMapMovementCancelMessage.class),
    INVENTORY_WEIGHT(3009, InventoryWeightMessage.class),
    INVENTORY_CONTENT(3016, InventoryContentMessage.class),
    FRIENDS_GET_LIST(4001, FriendsGetListMessage.class),
    FRIENDS_LIST(4002, FriendsListMessage.class),
    CHARACTER_NAME_SUGGESTION_SUCCESS(5544, CharacterNameSuggestionSuccessMessage.class),
    CLIENT_KEY(5607, ClientKeyMessage.class),
    QUEST_LIST_REQUEST(5623, QuestListRequestMessage.class),
    QUEST_LIST(5626, QuestListMessage.class),
    GAME_ROLE_PLAY_SHOW_ACTOR(5632, GameRolePlayShowActorMessage.class),
    EMOTE_LIST(5689, EmoteListMessage.class),
    IGNORED_LIST(5674, IgnoredListMessage.class),
    IGNORED_GET_LIST(5676, IgnoredGetListMessage.class),
    CHARACTER_SELECTED_ERROR(5836, CharacterSelectedErrorMessage.class),
    IDENTIFICATION_FAILED_BANNED(6174, IdentificationFailedBannedMessage.class),
    ACCOUNT_CAPABILITIES(6216, AccountCapabilitiesMessage.class),
    SHORTCUT_BAR_CONTENT(6231, ShortcutBarContentMessage.class),
    RAW_DATA(6253, RawDataMessage.class),
    SPOUSE_STATUS(6265, SpouseStatusMessage.class),
    TRUST_STATUS(6267, TrustStatusMessage.class),
    SERVER_OPTIONAL_FEATURES(6305, ServerOptionalFeaturesMessage.class),
    SERVER_SETTINGS(6340, ServerSettingsMessage.class),
    SPOUSE_GET_INFORMATION(6355, SpouseGetInformationMessage.class),
    SPOUSE_INFORMATION(6356, null),
    BASIC_ACK(6362, BasicAckMessage.class),
    CHECK_INTEGRITY(6372, null),
    CHARACTER_LOADING_COMPLETE(6471, CharacterLoadingCompleteMessage.class),
    RELOGIN_TOKEN_STATUS(6539, ReloginTokenStatusMessage.class),
    RELOGIN_TOKEN_REQUEST(6540, ReloginTokenRequestMessage.class),
    HAAPI_API_KEY_REQUEST(6648, HaapiApiKeyRequestMessage.class),;

    private int id;
    private Class<? extends D2Message> messageClass;

    D2MessageType(int id, Class<? extends D2Message> messageClass) {
        this.id = id;
        this.messageClass = messageClass;
    }

    public int getId() {
        return id;
    }

    public Class<? extends D2Message> getMessageClass() {
        return messageClass;
    }
}
