package org.crohemu.network.d2protocol.message;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpMessage;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

/**
 * The D2Message class encapsulates the Dofus 2 protocol.
 * Every message can be received on sent.
 */
public abstract class D2Message extends TcpMessage {

    /**
     * The message's high header length.
     */
    private static final int HIGH_HEADER_SIZE = 2 * Byte.BYTES;

    /**
     * The number of bytes needed to encode the message's length (1|2|3 bytes)
     */
    protected int lengthType;

    /**
     * the message's length in bytes.
     */
    protected int length;

    /**
     * Constructs the message from raw byte data (when it has been received).
     * @param messageData The raw socket data.
     */
    public D2Message(byte[] messageData) {
        super(messageData);

        unpack(messageData);

        D2DataInputStream inputStream = new D2DataInputStream(messageData, HIGH_HEADER_SIZE + this.lengthType, messageData.length);
        this.deserialize(inputStream);
    }

    private void unpack(byte[] messageData) {
        this.lengthType = messageData[1] & 0b00000011;
        switch (this.lengthType) {
            case 1:
                this.length = messageData[2];
                break;
            case 2:
                this.length = messageData[2] << 8 | messageData[3];
                break;
            case 3:
                this.length = messageData[2] << 16 | messageData[3] << 8 | messageData[4];
                break;
        }
    }

    public D2Message() {
        super();
    }

    /**
     * Extract the message content data from the raw bytes.
     */
    protected void deserialize(D2DataInputStream inputStream) {}

    /**
     * Packs the data as a byte array.
     * Implementations should finish by calling the <i>pack</i> method.
     */
    protected void serialize(D2DataOutputStream outputStream) {}

    /**
     * Computes and write the header of the message.
     */
    @Override
    public void pack() {
        D2DataOutputStream outputStream = new D2DataOutputStream();

        this.serialize(outputStream);

        ByteArrayOutputStream underlyingOutputStream = outputStream.getUnderlyingStream();

        this.length = underlyingOutputStream.size();
        this.lengthType = computeLengthType(this.length);

        rawContent = new byte[this.length + this.lengthType + HIGH_HEADER_SIZE];
        ByteBuffer byteBuffer = ByteBuffer.wrap(rawContent);

        int id = -1;
        for (D2MessageType type : D2MessageType.values()) {
            if (type.getMessageClass() == null) {
                continue;
            }
            if (type.getMessageClass().equals(this.getClass())) {
                id = type.getId();
                break;
            }
        }

        byte[] highHeader = new byte[2];
        highHeader[0] = (byte) (id >> 6);
        highHeader[1] = (byte) ((id << 2) | lengthType);

        byteBuffer.put(highHeader);

        switch (lengthType) {
            case 1:
                byteBuffer.put((byte) length);
                break;
            case 2:
                byteBuffer.putShort((short) length);
                break;
            case 3:
                byteBuffer.put((byte) (length >> 16));
                byteBuffer.put((byte) (length >> 8));
                byteBuffer.put((byte) length);
                break;
        }

        byteBuffer.put(underlyingOutputStream.toByteArray());
    }

    public int getLengthType() {
        return lengthType;
    }

    public int getLength() {
        return length;
    }

    /**
     * Calculates the message length type (number of bytes of the length).
     * @param length The message's length.
     * @return The message's length type.
     */
    protected int computeLengthType(int length) {
        if (length == 0) {
            return 0;
        } else if (length <= (Byte.MAX_VALUE - Byte.MIN_VALUE)) {
            return 1;
        } else if (length <= (Short.MAX_VALUE - Short.MIN_VALUE)) {
            return 2;
        } else {
            return 3;
        }
    }

    public boolean isValid() {
        return true;
    }
}
