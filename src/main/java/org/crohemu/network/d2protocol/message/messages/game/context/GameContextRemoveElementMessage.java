package org.crohemu.network.d2protocol.message.messages.game.context;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class GameContextRemoveElementMessage extends D2Message {

    private double id;

    public GameContextRemoveElementMessage(double id) {
        this.id = id;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeDouble(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setId(double id) {
        this.id = id;
    }
}
