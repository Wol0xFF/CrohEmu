package org.crohemu.network.d2protocol.message.messages.game.social;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.social.AbstractContactInformation;

import java.io.IOException;
import java.util.List;

public class IgnoredListMessage extends D2Message {

    private List<AbstractContactInformation> ignoredInformationList;

    public IgnoredListMessage() {

    }

    public IgnoredListMessage(List<AbstractContactInformation> ignoredInformationList) {
        this();
        this.ignoredInformationList = ignoredInformationList;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(ignoredInformationList.size());
            for (AbstractContactInformation info : ignoredInformationList) {
                info.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<AbstractContactInformation> getIgnoredInformationList() {
        return ignoredInformationList;
    }

    public void setIgnoredInformationList(List<AbstractContactInformation> ignoredInformationList) {
        this.ignoredInformationList = ignoredInformationList;
    }
}
