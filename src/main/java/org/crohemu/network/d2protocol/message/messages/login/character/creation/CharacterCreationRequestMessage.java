package org.crohemu.network.d2protocol.message.messages.login.character.creation;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CharacterCreationRequestMessage extends D2Message {

    private static final int COLOR_COUNT = 5;

    private String name;
    private int breed;
    private boolean sex;
    private List<Integer> colors;
    private int cosmeticId;

    public CharacterCreationRequestMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            name = inputStream.readUTF();
            breed = inputStream.readByte();
            sex = inputStream.readBoolean();

            colors = new ArrayList<>(COLOR_COUNT);
            for (int i = 0; i < COLOR_COUNT; i++) {
                    colors.add(inputStream.readInt());
            }
            cosmeticId = inputStream.readVariableLengthShort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return ((breed >= 1 && breed <= 17)
                && !colors.stream().anyMatch(color -> color < 0 || color > 16777216));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public List<Integer> getColors() {
        return colors;
    }

    public void setColors(List<Integer> colors) {
        this.colors = colors;
    }

    public int getCosmeticId() {
        return cosmeticId;
    }

    public void setCosmeticId(int cosmeticId) {
        this.cosmeticId = cosmeticId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("breed", breed)
                .append("sex", sex)
                .append("colors", colors)
                .append("cosmeticId", cosmeticId)
                .toString();
    }
}
