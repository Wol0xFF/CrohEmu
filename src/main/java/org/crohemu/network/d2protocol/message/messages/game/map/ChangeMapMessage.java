package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class ChangeMapMessage extends D2Message {

    private int mapId;

    public ChangeMapMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            mapId = inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getMapId() {
        return mapId;
    }
}
