package org.crohemu.network.d2protocol.message.messages.game.auth;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;

public class TrustStatusMessage extends D2Message {
    private boolean trusted;
    private boolean certified;

    public TrustStatusMessage(boolean trusted, boolean certified) {
        this.trusted = trusted;
        this.certified = certified;

    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.write(BooleanFlagsWriter.flagsToByte(trusted, certified));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
