package org.crohemu.network.d2protocol.message.messages.types.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;
import java.util.List;

public class InteractiveElement implements D2NetworkType {
    private int elementId;
    private int elementTypeId;
    private List<InteractiveElementSkill> enabledSkills;
    private List<InteractiveElementSkill> disabledSkills;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(elementId);
            outputStream.writeShort(enabledSkills.size());
            for (InteractiveElementSkill enabledSkill : enabledSkills) {
                outputStream.writeShort(enabledSkill.getTypeId());
                enabledSkill.serialize(outputStream);
            }
            outputStream.writeShort(disabledSkills.size());
            for (InteractiveElementSkill disabledSkill : disabledSkills) {
                outputStream.writeShort(disabledSkill.getTypeId());
                disabledSkill.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return true;
    }
}
