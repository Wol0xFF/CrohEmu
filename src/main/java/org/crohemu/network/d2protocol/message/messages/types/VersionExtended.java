package org.crohemu.network.d2protocol.message.messages.types;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

/**
 * The client version.
 */
public class VersionExtended implements D2NetworkType {

    private int install;
    private int technology;

    private int major;
    private int minor;
    private int release;
    private int revision;
    private int patch;
    private int buildType;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        // never sent to client
    }

    @Override
    public void deserialize(D2DataInputStream in) {
        try {
            this.major = in.readByte();
            this.minor = in.readByte();
            this.release = in.readByte();
            this.revision = in.readInt();
            this.patch = in.readByte();
            this.buildType = in.readByte();

            this.install = in.readByte();
            this.technology = in.readByte();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return major >= 0
                && minor >= 0
                && release >= 0
                && revision >= 0
                && patch >= 0
                && install >= 0
                && technology >= 0;
    }

    public int getInstall() {
        return install;
    }

    public int getTechnology() {
        return technology;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getRelease() {
        return release;
    }

    public int getRevision() {
        return revision;
    }

    public int getPatch() {
        return patch;
    }

    public int getBuildType() {
        return buildType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("install", install)
                .append("technology", technology)
                .append("major", major)
                .append("minor", minor)
                .append("release", release)
                .append("revision", revision)
                .append("patch", patch)
                .append("buildType", buildType)
                .toString();
    }
}
