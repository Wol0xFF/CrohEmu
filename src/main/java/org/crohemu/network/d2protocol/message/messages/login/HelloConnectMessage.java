package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * This message contains a special salt and key that authenticates the server.
 * They are in the login server property file. They were retrieved by analyzing the packets of a
 * successful identification.
 * It is likely that the official salt and key change with updates.
 */
public class HelloConnectMessage extends D2Message {

    private String salt;
    private byte[] key;

    public HelloConnectMessage(byte[] messageData) {
        super(messageData);

    }

    public HelloConnectMessage() {

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            this.salt = inputStream.readUTF();
            int keyLength = inputStream.readVariableLengthInt();

            this.key = new byte[keyLength];
            int bytesRead = inputStream.read(key);
            if (bytesRead != keyLength) {
                throw new IOException("Error while reading HelloConnect key : bad number of bytes read : " + bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeUTF(salt);
            outputStream.writeVariableLengthInt(key.length);
            outputStream.write(key);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }
}
