package org.crohemu.network.d2protocol.message.messages.types.fight;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class FightTeamMemberInformation implements D2NetworkType {

    private int id;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return true;
    }

    public int getTypeId() {
        return 44;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
