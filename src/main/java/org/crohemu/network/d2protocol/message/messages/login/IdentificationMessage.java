package org.crohemu.network.d2protocol.message.messages.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.VersionExtended;
import org.crohemu.network.d2protocol.utils.BooleanFlagsReader;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;

/**
 * Sent by the client on login. Contains the credentials, the client version...
 * Patched in the client. The client uses a proprietary hashing algorithm that is very hard
 * to retro-engineer. It has been modified to send the credentials in plain text, as an ascii code
 * array, where the account name and password are separated by a specific delimiter.
 */
public class IdentificationMessage extends D2Message {
    public static final String CREDENTIALS_DELIMITER = "\\|\\|";

    /**
     * TODO
     */
    private boolean autoConnect;

    /**
     * Because the client is patched, this has no use.
     */
    private boolean useCertificate;

    /**
     * TODO
     */
    private boolean useLoginToken;

    /**
     * The client's version.
     */
    private VersionExtended versionExtended;

    /**
     * The client's language abbreviation.
     */
    private String lang;

    /**
     * The credentials in ascii, separated by CREDENTIALS_DELIMITER.
     */
    private byte[] credentials;

    /**
     * TODO
     * Probably used when the client wants to bypass the server selection step.
     */
    private short serverId;

    /**
     * TODO
     */
    private long sessionOptionalSalt;

    /**
     * The number of times the identification failed. Prevents brute forcing the credentials.
     * TODO use it
     */
    private short[] failedAttempts;

    public IdentificationMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            byte flags = inputStream.readByte();
            this.autoConnect = BooleanFlagsReader.readFlag(flags, 0);
            this.useCertificate = BooleanFlagsReader.readFlag(flags, 1);
            this.useLoginToken = BooleanFlagsReader.readFlag(flags, 2);

            this.versionExtended = new VersionExtended();
            this.versionExtended.deserialize(inputStream);

            this.lang = inputStream.readUTF();

            int credentialsLength = inputStream.readVariableLengthInt();
            this.credentials = new byte[credentialsLength];
            for (int i = 0; i < credentialsLength; i++) {
                this.credentials[i] = inputStream.readByte();
            }

            this.serverId = inputStream.readShort();
            this.sessionOptionalSalt = inputStream.readVariableLengthLong();

            int failedAttemptsLength = inputStream.readUnsignedShort();
            this.failedAttempts = new short[failedAttemptsLength];
            for (int i = 0; i < failedAttemptsLength; i++) {
                failedAttempts[i] = inputStream.readVariableLengthShort();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return serverId >= 0 && versionExtended.isValid();
    }

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public boolean isUseCertificate() {
        return useCertificate;
    }

    public boolean isUseLoginToken() {
        return useLoginToken;
    }

    public VersionExtended getVersionExtended() {
        return versionExtended;
    }

    public String getLang() {
        return lang;
    }

    public byte[] getCredentials() {
        return credentials;
    }

    public short getServerId() {
        return serverId;
    }

    public long getSessionOptionalSalt() {
        return sessionOptionalSalt;
    }

    public short[] getFailedAttempts() {
        return failedAttempts;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("autoConnect", autoConnect)
                .append("useCertificate", useCertificate)
                .append("useLoginToken", useLoginToken)
                .append("versionExtended", versionExtended)
                .append("lang", lang)
                .append("credentials", credentials)
                .append("serverId", serverId)
                .append("sessionOptionalSalt", sessionOptionalSalt)
                .append("failedAttempts", failedAttempts)
                .toString();
    }
}
