package org.crohemu.network.d2protocol.message.messages.game;

import org.crohemu.network.d2protocol.message.D2Message;

public class GameContextCreateRequestMessage extends D2Message {
    public GameContextCreateRequestMessage(byte[] messageData) {
        super(messageData);
    }
}
