package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayActorInformation;
import org.crohemu.network.d2protocol.message.messages.types.fight.FightCommonInformation;
import org.crohemu.network.d2protocol.message.messages.types.map.HouseInformation;
import org.crohemu.network.d2protocol.message.messages.types.map.InteractiveElement;
import org.crohemu.network.d2protocol.message.messages.types.map.MapObstacle;
import org.crohemu.network.d2protocol.message.messages.types.map.StatedElement;

import java.io.IOException;
import java.util.List;

public class MapComplementaryInformationDataMessage extends D2Message {

    private short subAreaId;
    private int mapId;
    private List<HouseInformation> houses;
    private List<GameRolePlayActorInformation> actors;
    private List<InteractiveElement> interactiveElements;
    private List<StatedElement> statedElements;
    private List<MapObstacle> mapObstacles;
    private List<FightCommonInformation> fightCommonInformationList;
    private boolean hasAggressiveMonsters;

    public MapComplementaryInformationDataMessage() {
        super();
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthShort(subAreaId);
            outputStream.writeInt(mapId);
            outputStream.writeShort(houses.size());
            for (HouseInformation house : houses) {
                outputStream.writeShort(house.getTypeId());
                house.serialize(outputStream);
            }
            outputStream.writeShort(actors.size());
            for (GameRolePlayActorInformation actor : actors) {
                outputStream.writeShort(actor.getTypeId());
                actor.serialize(outputStream);
            }
            outputStream.writeShort(interactiveElements.size());
            for (InteractiveElement element : interactiveElements) {
                element.serialize(outputStream);
            }
            outputStream.writeShort(statedElements.size());
            for (StatedElement element : statedElements) {
                element.serialize(outputStream);
            }
            outputStream.writeShort(mapObstacles.size());
            for (MapObstacle obstacle : mapObstacles) {
                obstacle.serialize(outputStream);
            }
            outputStream.writeShort(fightCommonInformationList.size());
            for (FightCommonInformation fight : fightCommonInformationList) {
                fight.serialize(outputStream);
            }
            outputStream.writeBoolean(hasAggressiveMonsters);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public short getSubAreaId() {
        return subAreaId;
    }

    public void setSubAreaId(short subAreaId) {
        this.subAreaId = subAreaId;
    }

    public List<HouseInformation> getHouses() {
        return houses;
    }

    public void setHouses(List<HouseInformation> houses) {
        this.houses = houses;
    }

    public List<GameRolePlayActorInformation> getActors() {
        return actors;
    }

    public void setActors(List<GameRolePlayActorInformation> actors) {
        this.actors = actors;
    }

    public List<InteractiveElement> getInteractiveElements() {
        return interactiveElements;
    }

    public void setInteractiveElements(List<InteractiveElement> interactiveElements) {
        this.interactiveElements = interactiveElements;
    }

    public List<StatedElement> getStatedElements() {
        return statedElements;
    }

    public void setStatedElements(List<StatedElement> statedElements) {
        this.statedElements = statedElements;
    }

    public List<MapObstacle> getMapObstacles() {
        return mapObstacles;
    }

    public void setMapObstacles(List<MapObstacle> mapObstacles) {
        this.mapObstacles = mapObstacles;
    }

    public List<FightCommonInformation> getFightCommonInformationList() {
        return fightCommonInformationList;
    }

    public void setFightCommonInformationList(List<FightCommonInformation> fightCommonInformationList) {
        this.fightCommonInformationList = fightCommonInformationList;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public boolean isHasAggressiveMonsters() {
        return hasAggressiveMonsters;
    }

    public void setHasAggressiveMonsters(boolean hasAggressiveMonsters) {
        this.hasAggressiveMonsters = hasAggressiveMonsters;
    }
}
