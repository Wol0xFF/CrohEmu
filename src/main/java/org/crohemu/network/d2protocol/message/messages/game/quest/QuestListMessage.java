package org.crohemu.network.d2protocol.message.messages.game.quest;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.types.quest.QuestActiveInformation;

import java.io.IOException;
import java.util.List;

public class QuestListMessage extends D2Message {
    private List<Integer> finishedQuestsIds;
    private List<Integer> finishedQuestsCounts;
    private List<QuestActiveInformation> activeQuests;
    private List<Integer> reinitDoneQuestsIds;

    public QuestListMessage() {
        super();
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(finishedQuestsIds.size());
            for (Integer id : finishedQuestsIds) {
                outputStream.writeVariableLengthShort(id);
            }
            outputStream.writeShort(finishedQuestsCounts.size());
            for (Integer count : finishedQuestsCounts) {
                outputStream.writeVariableLengthShort(count);
            }
            outputStream.writeShort(activeQuests.size());
            for (QuestActiveInformation info : activeQuests) {
                info.serialize(outputStream);
            }
            outputStream.writeShort(reinitDoneQuestsIds.size());
            for (Integer id : reinitDoneQuestsIds) {
                outputStream.writeVariableLengthShort(id);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Integer> getFinishedQuestsIds() {
        return finishedQuestsIds;
    }

    public void setFinishedQuestsIds(List<Integer> finishedQuestsIds) {
        this.finishedQuestsIds = finishedQuestsIds;
    }

    public List<Integer> getFinishedQuestsCounts() {
        return finishedQuestsCounts;
    }

    public void setFinishedQuestsCounts(List<Integer> finishedQuestsCounts) {
        this.finishedQuestsCounts = finishedQuestsCounts;
    }

    public List<QuestActiveInformation> getActiveQuests() {
        return activeQuests;
    }

    public void setActiveQuests(List<QuestActiveInformation> activeQuests) {
        this.activeQuests = activeQuests;
    }

    public List<Integer> getReinitDoneQuestsIds() {
        return reinitDoneQuestsIds;
    }

    public void setReinitDoneQuestsIds(List<Integer> reinitDoneQuestsIds) {
        this.reinitDoneQuestsIds = reinitDoneQuestsIds;
    }
}
