package org.crohemu.network.d2protocol.message.messages.game.auth;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;

/**
 * Sent by the client after HelloGameMessage, to check that he has the right to connect to the game server.
 */
public class AuthenticationTicketMessage extends D2Message {

    private String lang;
    private String ticket;

    public AuthenticationTicketMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            lang = inputStream.readUTF();
            ticket = inputStream.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
