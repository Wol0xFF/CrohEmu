package org.crohemu.network.d2protocol.message.messages.types.actor;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityDispositionInformation;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityLook;

import java.io.IOException;

public class GameContextActorInformation implements D2NetworkType {

    private double contextualId;
    private EntityLook entityLook;
    private EntityDispositionInformation entityDispositionInformation;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeDouble(contextualId);
            entityLook.serialize(outputStream);
            outputStream.writeShort(entityDispositionInformation.getTypeId());
            entityDispositionInformation.serialize(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        //unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public int getTypeId() {
        return 150;
    }

    public double getContextualId() {
        return contextualId;
    }

    public void setContextualId(double contextualId) {
        this.contextualId = contextualId;
    }

    public EntityLook getEntityLook() {
        return entityLook;
    }

    public void setEntityLook(EntityLook entityLook) {
        this.entityLook = entityLook;
    }

    public EntityDispositionInformation getEntityDispositionInformation() {
        return entityDispositionInformation;
    }

    public void setEntityDispositionInformation(EntityDispositionInformation entityDispositionInformation) {
        this.entityDispositionInformation = entityDispositionInformation;
    }
}
