package org.crohemu.network.d2protocol.message.messages.game.emote;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;
import java.util.List;

public class EmoteListMessage extends D2Message {

    private List<Byte> emoteIds;

    public EmoteListMessage(List<Byte> emoteIds) {
        this.emoteIds = emoteIds;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(emoteIds.size());
            emoteIds.forEach(emoteId -> {
                try {
                    outputStream.writeByte(emoteId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Byte> getEmoteIds() {
        return emoteIds;
    }
}
