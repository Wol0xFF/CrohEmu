package org.crohemu.network.d2protocol.message.messages.common;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Sent by the client to check if the server is OK
 */
public class BasicPingMessage extends D2Message {
    private boolean quiet;

    public BasicPingMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            quiet = inputStream.readBoolean();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isQuiet() {
        return quiet;
    }
}
