package org.crohemu.network.d2protocol.message.messages.types.fight;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class AbstractFightTeamInformation implements D2NetworkType {

    private byte teamId;
    private int leaderId;
    private byte teamSide;
    private byte teamTypeId;
    private byte nbWaves;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(teamId);
            outputStream.writeInt(leaderId);
            outputStream.writeByte(teamSide);
            outputStream.writeByte(teamTypeId);
            outputStream.writeByte(nbWaves);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public int getTypeId() {
        return 116;
    }
}
