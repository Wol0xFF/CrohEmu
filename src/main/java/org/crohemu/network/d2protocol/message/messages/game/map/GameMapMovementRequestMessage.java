package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class GameMapMovementRequestMessage extends D2Message {
    private List<Short> keyMovements;
    private int mapId;

    public GameMapMovementRequestMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(keyMovements.size());
            for (Short movement : keyMovements) {
                outputStream.writeShort(movement);
            }
            outputStream.writeInt(mapId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            short keyMovementsCount = inputStream.readShort();
            keyMovements = new ArrayList<>(keyMovementsCount);
            IntStream.range(0, keyMovementsCount).forEach(i -> {
                try {
                    keyMovements.add(inputStream.readShort());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            mapId = inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Short> getKeyMovements() {
        return keyMovements;
    }

    public void setKeyMovements(List<Short> keyMovements) {
        this.keyMovements = keyMovements;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }
}
