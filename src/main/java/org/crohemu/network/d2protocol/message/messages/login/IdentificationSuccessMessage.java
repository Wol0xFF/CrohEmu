package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;

/**
 * Sent when the user's account has been successfully identified.
 */
public class IdentificationSuccessMessage extends D2Message {

    /**
     * Not implemented in the same way as in the official server.
     * Probably grants access to developer tools like the console.
     * TODO check this and use it
     */
    private boolean hasRights;

    /**
     * TODO
     */
    private boolean wasAlreadyConnected;

    /**
     * The account name.
     * @see org.crohemu.model.account.Account#accountName
     */
    private String login;

    /**
     * The account nickname.
     * @see org.crohemu.model.account.Account#nickname
     */
    private String nickname;

    /**
     * The functional account Id.
     * @see org.crohemu.model.account.Account#accountId
     */
    private int accountId;

    /**
     * The community Id.
     * @see org.crohemu.model.account.Account#community
     */
    private int communityId;

    /**
     * The secret question
     * @see org.crohemu.model.account.Account#secretQuestion
     */
    private String secretQuestion;

    /**
     * The account creation date (unix time)
     * @see org.crohemu.model.account.Account#creationDate
     */
    private double accountCreation;

    /**
     * The time left before the account loses its subscription.
     * @see org.crohemu.model.account.Account#subscription
     */
    private double subscriptionElapsedDuration;

    /**
     * The date (unix time) at which the subscription ends.
     * @see org.crohemu.model.account.Account#subscription
     */
    private double subscriptionEndDate;

    /**
     * TODO
     */
    private int havenbagAvailableRoom;

    public IdentificationSuccessMessage() {

    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.write(BooleanFlagsWriter.flagsToByte(hasRights, wasAlreadyConnected));
            outputStream.writeUTF(login);
            outputStream.writeUTF(nickname);
            outputStream.writeInt(accountId);
            outputStream.writeByte(communityId);
            outputStream.writeUTF(secretQuestion);
            outputStream.writeDouble(accountCreation);
            outputStream.writeDouble(subscriptionElapsedDuration);
            outputStream.writeDouble(subscriptionEndDate);
            outputStream.writeByte(havenbagAvailableRoom);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

    public boolean isHasRights() {
        return hasRights;
    }

    public void setHasRights(boolean hasRights) {
        this.hasRights = hasRights;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public double getAccountCreation() {
        return accountCreation;
    }

    public void setAccountCreation(double accountCreation) {
        this.accountCreation = accountCreation;
    }

    public double getSubscriptionElapsedDuration() {
        return subscriptionElapsedDuration;
    }

    public void setSubscriptionElapsedDuration(double subscriptionElapsedDuration) {
        this.subscriptionElapsedDuration = subscriptionElapsedDuration;
    }

    public double getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(double subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }

    public int getHavenbagAvailableRoom() {
        return havenbagAvailableRoom;
    }

    public void setHavenbagAvailableRoom(int havenbagAvailableRoom) {
        this.havenbagAvailableRoom = havenbagAvailableRoom;
    }
}
