package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

public class ReloginTokenRequestMessage extends D2Message {

    public ReloginTokenRequestMessage(byte[] messageData) {
        super(messageData);

    }
}
