package org.crohemu.network.d2protocol.message.messages.types.fight;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;

public class FightOptionsInformation implements D2NetworkType {

    private boolean isSecret;
    private boolean isRestrictedToPartyOnly;
    private boolean isClosed;
    private boolean isAskingForHelp;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(isSecret, isRestrictedToPartyOnly, isClosed, isAskingForHelp));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return true;
    }

    public boolean isSecret() {
        return isSecret;
    }

    public void setSecret(boolean secret) {
        isSecret = secret;
    }

    public boolean isRestrictedToPartyOnly() {
        return isRestrictedToPartyOnly;
    }

    public void setRestrictedToPartyOnly(boolean restrictedToPartyOnly) {
        isRestrictedToPartyOnly = restrictedToPartyOnly;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public boolean isAskingForHelp() {
        return isAskingForHelp;
    }

    public void setAskingForHelp(boolean askingForHelp) {
        isAskingForHelp = askingForHelp;
    }
}
