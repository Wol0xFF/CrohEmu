package org.crohemu.network.d2protocol.message.messages.common;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class BasicAckMessage extends D2Message {

    private int seq;
    private int lastPacketId;

    public BasicAckMessage(int seq, int lastPacketId) {
        this.seq = seq;
        this.lastPacketId = lastPacketId;


    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthInt(seq);
            outputStream.writeVariableLengthShort(lastPacketId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLastPacketId(int lastPacketId) {
        this.lastPacketId = lastPacketId;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }
}
