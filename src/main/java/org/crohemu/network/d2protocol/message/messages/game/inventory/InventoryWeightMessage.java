package org.crohemu.network.d2protocol.message.messages.game.inventory;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class InventoryWeightMessage extends D2Message {
    private int weight;
    private int maxWeight;

    public InventoryWeightMessage() {

    }

    public InventoryWeightMessage(int weight, int maxWeight) {
        this.weight = weight;
        this.maxWeight = maxWeight;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthInt(weight);
            outputStream.writeVariableLengthInt(maxWeight);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }
}
