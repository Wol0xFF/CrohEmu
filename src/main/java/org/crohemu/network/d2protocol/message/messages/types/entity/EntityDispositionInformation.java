package org.crohemu.network.d2protocol.message.messages.types.entity;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class EntityDispositionInformation implements D2NetworkType {

    private int cellId;
    private byte direction;

    public EntityDispositionInformation(int cellId, byte direction) {
        this.cellId = cellId;
        this.direction = direction;
    }

    public EntityDispositionInformation() {
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(cellId);
            outputStream.writeByte(direction);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public int getTypeId() { return 60; }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public byte getDirection() {
        return direction;
    }

    public void setDirection(byte direction) {
        this.direction = direction;
    }
}
