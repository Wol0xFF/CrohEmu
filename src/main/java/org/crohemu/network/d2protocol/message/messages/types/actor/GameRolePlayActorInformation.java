package org.crohemu.network.d2protocol.message.messages.types.actor;

import org.crohemu.network.d2protocol.D2NetworkType;

public class GameRolePlayActorInformation extends GameContextActorInformation implements D2NetworkType {

    @Override
    public int getTypeId() {
        return 141;
    }
}
