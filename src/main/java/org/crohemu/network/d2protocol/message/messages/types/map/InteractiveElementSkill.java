package org.crohemu.network.d2protocol.message.messages.types.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class InteractiveElementSkill implements D2NetworkType {

    private int skillId;
    private int skillInstanceUid;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthInt(skillId);
            outputStream.writeInt(skillInstanceUid);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return skillId >= 0 && skillInstanceUid >= 0;
    }

    public int getTypeId() { return 80; }
}
