package org.crohemu.network.d2protocol.message.messages.login.character.creation;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class CharacterNameSuggestionSuccessMessage extends D2Message {

    private String nameSuggestion;

    public CharacterNameSuggestionSuccessMessage(final String nameSuggestion) {
        super();
        this.nameSuggestion = nameSuggestion;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeUTF(this.nameSuggestion);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
