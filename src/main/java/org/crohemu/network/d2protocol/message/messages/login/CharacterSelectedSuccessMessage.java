package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.CharacterBaseInformation;

import java.io.IOException;

public class CharacterSelectedSuccessMessage extends D2Message {

    private CharacterBaseInformation information;
    private boolean isCollectingStats;

    public CharacterSelectedSuccessMessage(CharacterBaseInformation characterBaseInformation) {
        this.information = characterBaseInformation;


    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            information.serialize(outputStream);
            outputStream.writeBoolean(isCollectingStats);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CharacterBaseInformation getInformation() {
        return information;
    }

    public void setInformation(CharacterBaseInformation information) {
        this.information = information;
    }

    public boolean isCollectingStats() {
        return isCollectingStats;
    }

    public void setCollectingStats(boolean collectingStats) {
        isCollectingStats = collectingStats;
    }
}
