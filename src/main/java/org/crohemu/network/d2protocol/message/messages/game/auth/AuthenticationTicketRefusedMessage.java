package org.crohemu.network.d2protocol.message.messages.game.auth;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

/**
 * Sent to the client after an AuthenticationTicketMessage if the ticket is invalid.
 */
public class AuthenticationTicketRefusedMessage extends D2Message {
    public AuthenticationTicketRefusedMessage() {

    }
}
