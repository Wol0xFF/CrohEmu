package org.crohemu.network.d2protocol.message.messages.login.character;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class CharacterDeletionRequestMessage extends D2Message {

    long characterId;
    String secretAnswerHash;

    public CharacterDeletionRequestMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            characterId = inputStream.readVariableLengthLong();
            secretAnswerHash = inputStream.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(long characterId) {
        this.characterId = characterId;
    }

    public String getSecretAnswerHash() {
        return secretAnswerHash;
    }

    public void setSecretAnswerHash(String secretAnswerHash) {
        this.secretAnswerHash = secretAnswerHash;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("characterId", characterId)
                .append("secretAnswerHash", secretAnswerHash)
                .toString();
    }
}
