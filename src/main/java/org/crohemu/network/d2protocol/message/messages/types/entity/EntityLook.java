package org.crohemu.network.d2protocol.message.messages.types.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The look of a game entity, for example a character. Includes colors, face, etc.
 */
public class EntityLook implements D2NetworkType {
    private int bonesId = 0;
    private List<Integer> skins = new ArrayList<>();
    private List<Integer> indexedColors = new ArrayList<>();
    private List<Integer> scales = new ArrayList<>();
    private List<SubEntity> subEntities = new ArrayList<>();

    public int getBonesId() {
        return bonesId;
    }

    public void setBonesId(int bonesId) {
        this.bonesId = bonesId;
    }

    public List<Integer> getSkins() {
        return skins;
    }

    public void setSkins(List<Integer> skins) {
        this.skins = skins;
    }

    public List<Integer> getIndexedColors() {
        return indexedColors;
    }

    public void setIndexedColors(List<Integer> indexedColors) {
        this.indexedColors = indexedColors;
    }

    public List<Integer> getScales() {
        return scales;
    }

    public void setScales(List<Integer> scales) {
        this.scales = scales;
    }

    public List<SubEntity> getSubEntities() {
        return subEntities;
    }

    public void setSubEntities(List<SubEntity> subEntities) {
        this.subEntities = subEntities;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthShort(bonesId);
            outputStream.writeShort(skins.size());
            for (Integer skin : skins) {
                outputStream.writeVariableLengthShort(skin);
            }
            outputStream.writeShort(indexedColors.size());
            for (Integer indexedColor : indexedColors) {
                outputStream.writeInt(indexedColor);
            }
            outputStream.writeShort(scales.size());
            for (Integer scale : scales) {
                outputStream.writeVariableLengthShort(scale);
            }
            outputStream.writeShort(subEntities.size());
            for (SubEntity subEntity : subEntities) {
                subEntity.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // for testing purposes only

        try {
            this.bonesId = inputStream.readVariableLengthShort();
            int skinCount = inputStream.readShort();
            this.skins = new ArrayList<>(skinCount);
            for (int i = 0; i < skinCount; i++) {
                skins.add((int) inputStream.readVariableLengthShort());
            }
            int indexedColorsCount = inputStream.readShort();
            this.indexedColors = new ArrayList<>(indexedColorsCount);
            for (int i = 0; i < indexedColorsCount; i++) {
                this.indexedColors.add(inputStream.readInt());
            }
            int scalesCount = inputStream.readShort();
            this.scales = new ArrayList<>(scalesCount);
            for (int i = 0; i < scalesCount; i++) {
                this.scales.add((int) inputStream.readVariableLengthShort());
            }
            int subEntityCount = inputStream.readShort();
            this.subEntities = new ArrayList<>(subEntityCount);
            for (int i = 0; i < subEntityCount; i++) {
                SubEntity subEntity = new SubEntity();
                subEntity.deserialize(inputStream);
                this.subEntities.add(subEntity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return bonesId >= 0
                && skins.stream().allMatch(s -> s >= 0)
                && indexedColors.stream().allMatch(c -> c >= 0)
                && scales.stream().allMatch(s -> s > 0)
                && subEntities.stream().allMatch(D2NetworkType::isValid);
    }

    public static EntityLook parse(String s) {

        EntityLook entityLook = new EntityLook();
        Matcher matcher = Pattern.compile("^\\{([0-9]+)\\|([0-9]+)\\|\\|([0-9]+)\\}$").matcher(s);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid EntityLook String.");
        }

        entityLook.setBonesId(Integer.valueOf(matcher.group(1)));
        entityLook.setSkins(new ArrayList<>(Collections.singleton(Integer.valueOf(matcher.group(2)))));
        entityLook.setScales(new ArrayList<>(Collections.singleton(Integer.valueOf(matcher.group(3)))));

        return entityLook;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("bonesId", bonesId)
                .append("skins", skins)
                .append("indexedColors", indexedColors)
                .append("scales", scales)
                .append("subEntities", subEntities)
                .toString();
    }
}
