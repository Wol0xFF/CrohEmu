package org.crohemu.network.d2protocol.message.messages.types.quest;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class QuestActiveInformation implements D2NetworkType {
    public int questId = 0;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthShort(questId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        try {
            questId = inputStream.readVariableLengthShort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return questId >= 0;
    }
}
