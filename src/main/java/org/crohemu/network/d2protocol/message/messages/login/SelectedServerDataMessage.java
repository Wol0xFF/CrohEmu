package org.crohemu.network.d2protocol.message.messages.login;


import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;

/**
 * Sent to the client after a ServerSelectionMessage.
 * After having received this, the client will attempt to connect to the game server.
 */
public class SelectedServerDataMessage extends D2Message{

    private int serverId;
    private String address;

    private int port;

    private boolean canCreateNewCharacter;
    private int[] ticket;

    public SelectedServerDataMessage() {

    }

    public static int[] randomTicket() {
        final int TICKET_LENGTH = 256;

        int[] result = new int[TICKET_LENGTH];
        for(int i = 0; i < TICKET_LENGTH; i++) {
            result[i] = (int) Math.floor(Math.random() * 10);
        }
        return result;
    }

    public SelectedServerDataMessage(byte[] rawContent, TcpClient tcpClient) {
        super(rawContent);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        // never sent by client. for testing purposes.
        try {
            serverId = inputStream.readVariableLengthShort();
            address = inputStream.readUTF();
            port = inputStream.readUnsignedShort();
            canCreateNewCharacter = inputStream.readBoolean();
            ticket = new int[inputStream.readVariableLengthInt()];
            for (int i = 0; i < ticket.length; i++) {
                ticket[i] = inputStream.readByte();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthShort(serverId);
            outputStream.writeUTF(address);
            outputStream.writeShort(port);
            outputStream.writeBoolean(canCreateNewCharacter);
            outputStream.writeVariableLengthInt(ticket.length);
            for (int ticketByte : ticket) {
                outputStream.writeByte(ticketByte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setCanCreateNewCharacter(boolean canCreateNewCharacter) {
        this.canCreateNewCharacter = canCreateNewCharacter;
    }

    public void setTicket(int[] ticket) {
        this.ticket = ticket;
    }

    public int getServerId() {
        return serverId;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public boolean canCreateNewCharacter() {
        return canCreateNewCharacter;
    }

    public int[] getTicket() {
        return ticket;
    }
}
