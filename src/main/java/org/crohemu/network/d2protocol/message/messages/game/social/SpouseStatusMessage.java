package org.crohemu.network.d2protocol.message.messages.game.social;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class SpouseStatusMessage extends D2Message {
    private final boolean hasSpouse;

    public SpouseStatusMessage(boolean hasSpouse) {
        super();

        this.hasSpouse = hasSpouse;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeBoolean(hasSpouse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isHasSpouse() {
        return hasSpouse;
    }
}
