package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Sends the game server's settings to the client.
 */
public class ServerSettingsMessage extends D2Message {

    private String lang;
    private int community;
    private int gameType;
    private int arenaLeaveBanTime;

    public ServerSettingsMessage() {

    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeUTF(lang);
            outputStream.writeByte(community);
            outputStream.writeByte(gameType);
            outputStream.writeVariableLengthShort(arenaLeaveBanTime);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setCommunity(int community) {
        this.community = community;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }

    public void setArenaLeaveBanTime(int arenaLeaveBanTime) {
        this.arenaLeaveBanTime = arenaLeaveBanTime;
    }
}
