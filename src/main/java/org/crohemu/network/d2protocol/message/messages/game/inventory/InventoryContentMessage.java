package org.crohemu.network.d2protocol.message.messages.game.inventory;

import org.crohemu.model.character.inventory.InventoryItem;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;
import java.util.List;

public class InventoryContentMessage extends D2Message {

    private final List<InventoryItem> items;
    private final int kamas;

    public InventoryContentMessage() {

        items = null;
        kamas = 0;
    }

    public InventoryContentMessage(List<InventoryItem> items, int kamas) {
        this.items = items;
        this.kamas = kamas;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(items.size());
            items.forEach(item -> item.serialize(outputStream));
            outputStream.writeVariableLengthInt(kamas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<InventoryItem> getItems() {
        return items;
    }

    public int getKamas() {
        return kamas;
    }
}
