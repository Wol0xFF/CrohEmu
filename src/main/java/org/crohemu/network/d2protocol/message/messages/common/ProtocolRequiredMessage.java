package org.crohemu.network.d2protocol.message.messages.common;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Sent to the client at the very beginning, to check if it speaks the same language.
 */
public final class ProtocolRequiredMessage extends D2Message {

    /**
     * The protocol's required version.
     */
    private int requiredVersion;

    /**
     * The protocol's current version.
     * Set in the login server configuration file.
     */
    private int currentVersion;

    public ProtocolRequiredMessage(byte[] messageData) {
        super(messageData);

    }

    public ProtocolRequiredMessage(int requiredVersion, int currentVersion) {
        super();

        this.requiredVersion = requiredVersion;
        this.currentVersion = currentVersion;
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {

        try {
            this.requiredVersion = inputStream.readInt();
            this.currentVersion = inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(requiredVersion);
            outputStream.writeInt(currentVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getRequiredVersion() {
        return requiredVersion;
    }

    public int getCurrentVersion() {
        return currentVersion;
    }

    public void setRequiredVersion(int requiredVersion) {
        this.requiredVersion = requiredVersion;
    }

    public void setCurrentVersion(int currentVersion) {
        this.currentVersion = currentVersion;
    }
}