package org.crohemu.network.d2protocol.message.messages.types;

public enum IdentificationFailedReason {
    INVALID_CREDENTIALS(2),
    BANNED(3),
    KICKED(4);

    private int code;

    IdentificationFailedReason(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
