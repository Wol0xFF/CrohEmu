package org.crohemu.network.d2protocol.message.messages.game;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

/**
 * Sent to the client after ProtocolRequired. So basic it has no content.
 */
public class HelloGameMessage extends D2Message {
    public HelloGameMessage() {

    }
}
