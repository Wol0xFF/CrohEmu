package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class CharacterSelectionMessage extends D2Message {

    private long id;

    public CharacterSelectionMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            id = inputStream.readVariableLengthLong();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
