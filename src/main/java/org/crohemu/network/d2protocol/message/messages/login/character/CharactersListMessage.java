package org.crohemu.network.d2protocol.message.messages.login.character;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.CharacterBaseInformation;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Sent after a CharactersListRequestMessage.
 */
public class CharactersListMessage extends D2Message {

    private List<CharacterBaseInformation> characters;

    private boolean hasStartupActions;

    public CharactersListMessage(byte[] rawData, TcpClient source) {
        // for testing purposes only
        super(rawData);

    }

    public CharactersListMessage() {

        this.hasStartupActions = false;
        this.characters = new ArrayList<>();
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(characters.size());
            for (CharacterBaseInformation characterBaseInformation : characters) {
                outputStream.writeShort(CharacterBaseInformation.TYPE_ID);
                characterBaseInformation.serialize(outputStream);
            }
            outputStream.writeBoolean(hasStartupActions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        // only for testing purposes
        try {
            int characterCount = inputStream.readShort();
            this.characters = new ArrayList<>(characterCount);
            for (int i = 0; i < characterCount; i++) {
                inputStream.readShort(); // discarded
                CharacterBaseInformation characterBaseInformation = new CharacterBaseInformation();
                characterBaseInformation.deserialize(inputStream);
                this.characters.add(characterBaseInformation);
            }
            this.hasStartupActions = inputStream.readBoolean();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<CharacterBaseInformation> getCharacters() {
        return characters;
    }

    public void setCharacters(List<CharacterBaseInformation> characters) {
        this.characters = characters;
    }

    public boolean hasStartupActions() {
        return hasStartupActions;
    }

    public void setHasStartupActions(boolean hasStartupActions) {
        this.hasStartupActions = hasStartupActions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("characters", characters)
                .append("hasStartupActions", hasStartupActions)
                .toString();
    }
}
