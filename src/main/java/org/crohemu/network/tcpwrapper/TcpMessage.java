package org.crohemu.network.tcpwrapper;

public class TcpMessage {

    protected byte[] rawContent;

    public TcpMessage(byte[] rawContent) {
        this.rawContent = rawContent;
    }

    public TcpMessage() {
    }

    public byte[] getRawContent() {
        return rawContent;
    }

    public void pack() {
    }
}
