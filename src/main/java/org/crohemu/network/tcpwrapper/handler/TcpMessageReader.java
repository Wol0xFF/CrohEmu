package org.crohemu.network.tcpwrapper.handler;

import java.io.IOException;
import java.io.InputStream;

@FunctionalInterface
public interface TcpMessageReader {
    byte[] readTcpMessage(InputStream dataStream) throws IOException;
}
