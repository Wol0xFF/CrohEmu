package org.crohemu.network.tcpwrapper;

@FunctionalInterface
public interface TcpMessageHandler {
    void handleTcpMessage(byte[] messageData);
}
