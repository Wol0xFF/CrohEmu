package org.crohemu.network.tcpwrapper;

@FunctionalInterface
public interface TcpClientDisconnectionHandler {
    void handleTcpClientDisconnection(TcpClient client);
}
