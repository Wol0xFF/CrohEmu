package org.crohemu.network.tcpwrapper;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Set;

/**
 * Very basic TCP server that transmits messages to callbacks
 */
public interface TcpServer {

    /**
     * Starts the server. It begins accepting connections and redirecting messages.
     * @param ipAddress The local ip address
     * @param portNumber The port number to which the server will listen
     */
    void start(InetAddress ipAddress, int portNumber) throws IOException;

    void disconnectClient(TcpClient client);

    Set<TcpClient> getClients();
}
