package org.crohemu;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.SimpleLayout;
import org.crohemu.controller.config.ControllerBeanConfiguration;
import org.crohemu.persistence.config.PersistenceBeanConfiguration;
import org.crohemu.server.config.ServerBeanConfiguration;
import org.crohemu.server.game.GameServer;
import org.crohemu.server.login.LoginServer;
import org.crohemu.service.config.ServiceBeanConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.*;

/**
 * Main class of the program.
 *
 * @author Croh
 */
public class CrohEmu {
    public static void main(String[] args) throws IOException, InterruptedException {

        displayBanner();

        // configure log4j
        boolean showDetailedLog = System.getProperty("showDetailedLog", "false").equals("true");
        if (!showDetailedLog) {
            BasicConfigurator.configure(new FileAppender(new SimpleLayout(), "server.log"));
        } else {
            BasicConfigurator.configure();
        }

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(
                ServerBeanConfiguration.class,
                PersistenceBeanConfiguration.class,
                ServiceBeanConfiguration.class,
                ControllerBeanConfiguration.class);
        new ApplicationContextProvider().setApplicationContext(applicationContext);

        LoginServer loginServer = applicationContext.getBean(LoginServer.class);
        loginServer.start();
        GameServer gameServer = applicationContext.getBean(GameServer.class);
        gameServer.start();
    }

    private static void displayBanner() throws IOException, InterruptedException {
        ClassPathResource banner = new ClassPathResource("banner.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(banner.getInputStream()));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
            Thread.sleep(500);
        }
        System.out.printf("%n%n%n");
    }
}
