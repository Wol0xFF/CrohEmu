package org.crohemu;


import org.crohemu.admin.AdminCommandFactory;
import org.crohemu.controller.admin.AdminCommandController;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.PrintStream;
import java.util.Scanner;

@Component
public class ServerConsole {

    @Inject
    AdminCommandController adminCommandController;

    @Inject
    AdminCommandFactory adminCommandFactory;

    private PrintStream out;

    public ServerConsole() {
        out = System.out;
    }

    public void handleCommands() {
        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            adminCommandController.dispatchCommand(adminCommandFactory.buildCommand(command));
        }).start();
    }

    public synchronized void info(String format, Object... arguments) {
        out.printf("[INFO] " + format, arguments);
    }

    public synchronized void error(String format, Object... arguments) {
        out.printf("[ERROR] " + format, arguments);
    }
}

