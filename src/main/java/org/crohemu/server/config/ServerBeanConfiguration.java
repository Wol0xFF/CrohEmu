package org.crohemu.server.config;

import org.crohemu.ServerConsole;
import org.crohemu.controller.MessageControllerRoutes;
import org.crohemu.network.d2protocol.D2MessageDispatcher;
import org.crohemu.network.d2protocol.message.factory.D2MessageFactory;
import org.crohemu.network.d2protocol.message.factory.impl.D2MessageFactoryImpl;
import org.crohemu.server.D2MessageDispatcherImpl;
import org.crohemu.server.D2TcpServer;
import org.crohemu.server.game.GameServer;
import org.crohemu.server.login.LoginServer;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Spring Bean configuration.
 *
 * @author Croh
 */
@Configuration
public class ServerBeanConfiguration {

    @Bean
    ServerConsole serverConsole() { return new ServerConsole(); }

    @Bean
    LoginServer loginServer() {
        return new LoginServer();
    }

    @Bean
    GameServer gameServer() { return new GameServer(); }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    D2TcpServer d2TcpServer() { return new D2TcpServer(); }

    @Bean
    D2MessageDispatcher d2MessageDispatcher() { return new D2MessageDispatcherImpl(); }

    @Bean
    MessageControllerRoutes messageControllerRoutes() { return new MessageControllerRoutes(); }

    @Bean
    D2MessageFactory d2MessageFactory() { return new D2MessageFactoryImpl(); }
}
