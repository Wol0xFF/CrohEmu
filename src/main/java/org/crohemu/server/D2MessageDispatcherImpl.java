package org.crohemu.server;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import org.crohemu.controller.MessageController;
import org.crohemu.controller.MessageControllerRoutes;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.D2MessageDispatcher;
import org.crohemu.network.d2protocol.message.D2Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class D2MessageDispatcherImpl implements D2MessageDispatcher {

    Logger logger = LoggerFactory.getLogger(D2MessageDispatcherImpl.class);

    @Inject
    MessageControllerRoutes messageControllerRoutes;

    @Override
    public void dispatchD2Message(D2Message message, D2Client client) {
        if (message == null) {
            return; // invalid message
        }

        if (!message.isValid()) {
            logger.error("Invalid message received! Client: {}, message: {}", client, message);
            return;
        }
        logger.debug("[{}] <--- {} ({})", client, message.getClass().getSimpleName(), HexBin.encode(message.getRawContent()));

        messageControllerRoutes.initialize();
        MessageController controller = messageControllerRoutes.get(message.getClass());
        if (controller == null) {
            logger.error("Controller not defined for message type {}", message.getClass());
        } else {
            controller.handleMessage(message, client);
        }
    }
}
