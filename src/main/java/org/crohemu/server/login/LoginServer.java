package org.crohemu.server.login;

import org.crohemu.server.D2ServerImpl;
import org.crohemu.server.login.config.LoginServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;

@Component
public class LoginServer extends D2ServerImpl {
    private final Logger logger = LoggerFactory.getLogger(LoginServer.class);

    public LoginServer() {
        super(Type.LOGIN);
    }

    public void start() {
        try {
            this.start(InetAddress.getByName(LoginServerConfig.getIpAddress()), LoginServerConfig.getPortNumber());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
