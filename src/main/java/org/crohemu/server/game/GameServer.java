package org.crohemu.server.game;

import org.crohemu.model.character.Character;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.server.D2ServerImpl;
import org.crohemu.server.game.config.GameServerConfig;
import org.crohemu.server.login.LoginServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GameServer extends D2ServerImpl {
    private final Logger logger = LoggerFactory.getLogger(GameServer.class);

    private Set<D2Client> futureClients = new HashSet<>();

    @Inject
    LoginServer loginServer;

    public GameServer() {
        super(Type.GAME);
    }

    public void start() {
        try {
            this.start(InetAddress.getByName(GameServerConfig.getIpAddress()), GameServerConfig.getPortNumber());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a D2Client that is supposed to connect to the game server very shortly.
     * This allows the game server to retrieve the client's account and stuff.
     * @param client
     */
    public void addFutureClient(D2Client client) {
        this.futureClients.add(client);
    }

    public boolean authenticateClient(D2Client client, String ticket) {

        Iterator<D2Client> futureClientIterator = futureClients.iterator();
        while (futureClientIterator.hasNext()) {
            D2Client futureClient = futureClientIterator.next();
            StringBuilder futureClientAuthTicketStringBuilder = new StringBuilder();
            for (int ticketElem : futureClient.getAuthTicket()) {
                futureClientAuthTicketStringBuilder.append(ticketElem);
            }
            String futureClientAuthTicket = futureClientAuthTicketStringBuilder.toString();

            if (futureClientAuthTicket.equals(ticket)) {
                client.setAccount(futureClient.getAccount());
                client.setIdentified(futureClient.isIdentified());
                client.setAuthTicket(futureClient.getAuthTicket());
                client.setAuthenticated(true);
                futureClientIterator.remove();
                return true;
            }
        }

        return false;
    }

    public Set<Character> getCharactersInGame() {
        return this.getD2Clients().stream()
                .map(D2Client::getCurrentCharacter)
                .filter(character -> character != null)
                .collect(Collectors.toSet());
    }

    public void sendMap(int mapId, D2Message message) {
        this.getD2Clients().stream()
                .filter(D2Client::isAuthenticated)
                .filter(client -> client.getCurrentCharacter().getCurrentMapId() == mapId)
                .forEach(client -> client.sendMessage(message));
    }
}
