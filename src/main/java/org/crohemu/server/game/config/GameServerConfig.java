package org.crohemu.server.game.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * The game server's configuration.
 * Some values are read from property files, and others are hardcoded constants.
 * Stops the program if the values contained in the property files do not pass the sanity checks.
 */
public class GameServerConfig {

    private static Logger logger = LoggerFactory.getLogger(GameServerConfig.class);

    private static String ipAddress = null;
    private static Integer portNumber = null;
    private static Integer protocolVersion = null;
    private static Integer serverId = null;
    private static Integer arenaLeaveBanTime = null;
    private static Integer community = null;
    private static Integer type = null;
    private static String lang = null;
    private static Long date = null;
    private static Integer completion = null;
    private static Integer charactersSlots = null;

    private static final String CONFIG_FILE_PATH = "./config/gameserver.properties";

    private static Properties configProperties;

    // Loads the config file then reads it
    static {
        try {
            configProperties = PropertiesLoaderUtils.loadProperties(new FileSystemResource(CONFIG_FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }

        readProperties();
    }

    private static void readProperties() {

        ipAddress = readIpAddress();
        portNumber = readPortNumber();
        protocolVersion = readProtocolVersion();
        serverId = readServerId();
        arenaLeaveBanTime = readArenaLeaveBanTime();
        community = readCommunity();
        type = readType();
        lang = readLang();
        date = readDate();
        completion = readCompletion();
        charactersSlots = readCharactersSlots();
    }

    private static Integer readCharactersSlots() {
        final String PROP_NAME = "charactersSlots";

        Integer charactersSlots = Integer.valueOf(configProperties.getProperty(PROP_NAME));

        if (charactersSlots < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return charactersSlots;
    }

    private static Integer readCompletion() {
        final String PROP_NAME = "completion";

        Integer completion = Integer.valueOf(configProperties.getProperty(PROP_NAME));

        if (completion < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return completion;
    }

    private static Long readDate() {
        final String PROP_NAME = "date";

        Long date = Long.valueOf(configProperties.getProperty(PROP_NAME));

        if (date < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return date;
    }

    private static String readLang() {
        final String PROP_NAME = "lang";

        String lang = configProperties.getProperty(PROP_NAME);

        if (StringUtils.isBlank(lang)) {
            handleInvalidProperty(PROP_NAME);
        }

        return lang;
    }

    private static Integer readType() {
        final String PROP_NAME = "type";
        return Integer.valueOf(configProperties.getProperty(PROP_NAME));
    }

    private static Integer readCommunity() {
        final String PROP_NAME = "community";

        Integer community = Integer.valueOf(configProperties.getProperty(PROP_NAME));

        if (community < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return community;
    }

    private static Integer readArenaLeaveBanTime() {
        final String PROP_NAME = "arenaLeaveBanTime";

        Integer arenaLeaveBanTime = Integer.valueOf(configProperties.getProperty(PROP_NAME));

        if (arenaLeaveBanTime < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return arenaLeaveBanTime;
    }

    private static Integer readServerId() {
        final String PROP_NAME = "serverId";

        Integer serverId = Integer.valueOf(configProperties.getProperty(PROP_NAME));

        if (serverId < 0) {
            handleInvalidProperty(PROP_NAME);
        }

        return serverId;
    }

    private static Integer readProtocolVersion() {
        final String PROP_NAME = "protocolVersion";
        
        Integer protocolVersion = Integer.valueOf(configProperties.getProperty(PROP_NAME));
        
        if (protocolVersion < 0) {
            handleInvalidProperty(PROP_NAME);
        }
        
        return protocolVersion;
    }

    private static String readIpAddress() {
        final String PROP_NAME = "ipAddress";

        String ipAddress = configProperties.getProperty(PROP_NAME);

        if (ipAddress == null) {
            handleInvalidProperty(PROP_NAME);
        }

        return ipAddress;
    }

    private static Integer readPortNumber() {
        String PROP_NAME = "portNumber";
        int portNumber = Integer.parseInt(configProperties.getProperty(PROP_NAME));

        if (portNumber <= 1024 || portNumber >= 65536) {
            handleInvalidProperty(PROP_NAME);
        }

        return portNumber;
    }

    private static void handleInvalidProperty(String propName) {
        logger.error("invalid value for property {} in config file {}", propName, CONFIG_FILE_PATH);
    }

    public static String getIpAddress() {
        return ipAddress;
    }

    public static int getPortNumber() {
        return portNumber;
    }

    public static Integer getProtocolVersion() {
        return protocolVersion;
    }

    public static Integer getServerId() {
        return serverId;
    }

    public static int getArenaLeaveBanTime() {
        return arenaLeaveBanTime;
    }

    public static int getCommunity() {
        return community;
    }

    public static int getType() {
        return type;
    }

    public static String getLang() {
        return lang;
    }

    public static Long getDate() {
        return date;
    }

    public static Integer getCompletion() {
        return completion;
    }

    public static Integer getCharactersSlots() {
        return charactersSlots;
    }
}
