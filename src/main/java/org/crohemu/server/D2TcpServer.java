package org.crohemu.server;

import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.D2MessageDispatcher;
import org.crohemu.network.d2protocol.D2MessageReader;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.factory.D2MessageFactory;
import org.crohemu.network.d2protocol.message.messages.common.ProtocolRequiredMessage;
import org.crohemu.network.tcpwrapper.TcpClient;
import org.crohemu.network.tcpwrapper.impl.TcpServerImpl;
import org.crohemu.server.game.config.GameServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class D2TcpServer extends TcpServerImpl {

    private final Logger logger = LoggerFactory.getLogger(D2TcpServer.class);

    private Set<D2Client> d2Clients = new HashSet<>();

    private D2MessageReader messageReader = new D2MessageReader();

    @Inject
    D2MessageFactory d2MessageFactory;

    @Inject
    D2MessageDispatcher d2MessageDispatcher;

    protected D2Message helloMessage;

    @Override
    protected void onClientConnected(TcpClient client) {
        super.onClientConnected(client);

        logger.info("new connection: {}", client);

        D2Client d2Client = new D2Client(client);
        d2Clients.add(d2Client);

        int protocolVersion = GameServerConfig.getProtocolVersion();
        d2Client.sendMessage(new ProtocolRequiredMessage(protocolVersion, protocolVersion));
        d2Client.sendMessage(this.helloMessage);
    }

    @Override
    protected void onClientClosedConnection(TcpClient client) {
        super.onClientClosedConnection(client);
        this.disconnectClient(this.findD2Client(client));
        logger.info("Client {} closed connection", client);
    }

    @Override
    protected byte[] readMessage(InputStream socketStream) throws IOException {
        return messageReader.readTcpMessage(socketStream);
    }

    @Override
    protected void handleMessage(byte[] messageData, TcpClient source) {
        super.handleMessage(messageData, source);
        D2Message d2Message = d2MessageFactory.createMessage(messageData);
        d2MessageDispatcher.dispatchD2Message(d2Message, this.findD2Client(source));
    }

    private D2Client findD2Client(TcpClient tcpClient) {
        return this.d2Clients.stream()
                .filter(d2Client -> d2Client.getTcpClient() == tcpClient)
                .collect(Collectors.toList())
                .get(0);
    }

    public void disconnectClient(D2Client d2client) {
        this.disconnectClient(d2client.getTcpClient());
        if (!d2Clients.remove(d2client)) {
            logger.warn("Tried to remove client {} but it is not registered on this server", d2client);
        }
    }

    public Set<D2Client> getD2Clients() {
        return d2Clients;
    }

    public void setHelloMessage(D2Message helloMessage) {
        this.helloMessage = helloMessage;
    }
}
