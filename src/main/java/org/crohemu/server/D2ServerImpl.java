package org.crohemu.server;

import org.crohemu.model.account.Account;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.HelloGameMessage;
import org.crohemu.network.d2protocol.message.messages.login.HelloConnectMessage;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.server.login.config.LoginServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Set;
import java.util.stream.Collectors;

public class D2ServerImpl implements D2Server {

    private final Logger logger = LoggerFactory.getLogger(D2ServerImpl.class);

    @Inject
    private D2TcpServer d2TcpServer;

    private final Type type;

    public D2ServerImpl(Type type) {
        this.type = type;
    }

    @PostConstruct
    private void postConstruct() {
        if (type == Type.LOGIN) {
            HelloConnectMessage helloMessage = new HelloConnectMessage();
            helloMessage.setKey(LoginServerConfig.getDofusPublicKey());
            helloMessage.setSalt(LoginServerConfig.getDofusPublicKeySalt());
            d2TcpServer.setHelloMessage(helloMessage);
        } else if (type == Type.GAME) {
            d2TcpServer.setHelloMessage(new HelloGameMessage());
        }
    }

    @Override
    public Set<D2Client> getD2Clients() {
        return d2TcpServer.getD2Clients();
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void start(InetAddress ipAddress, int portNumber) throws IOException {
        logger.info("Starting {} server", type);
        d2TcpServer.start(ipAddress, portNumber);
    }

    @Override
    public void disconnectClient(D2Client d2Client) {
        logger.info("Disconnecting client {} from {} server", d2Client, type);
        d2TcpServer.disconnectClient(d2Client);
    }

    @Override
    public Set<D2Client> getClientsConnectedOnAccount(Account account) {
        return d2TcpServer.getD2Clients().stream()
                .filter(d2Client -> d2Client.getAccount() != null)
                .filter(d2Client -> d2Client.getAccount().getAccountId() == account.getAccountId())
                .collect(Collectors.toSet());
    }
}
