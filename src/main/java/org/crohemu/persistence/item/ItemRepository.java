package org.crohemu.persistence.item;

import org.crohemu.model.item.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends MongoRepository<Item, String> {

    Item findById(int id);
}
