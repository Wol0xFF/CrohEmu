package org.crohemu.persistence.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import org.bson.BSON;
import org.bson.types.BSONTimestamp;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collections;

@Configuration
@EnableMongoRepositories(basePackages = "org.crohemu.persistence")
public class PersistenceBeanConfiguration {
    private static Logger logger = LoggerFactory.getLogger(PersistenceBeanConfiguration.class);

    @Bean
    public MongoClient mongoClient() {
        // add encoding hooks for Date and DateTime.
        BSON.addEncodingHook(Instant.class, new JodaTimeTransformer());
        BSON.addDecodingHook(BSONTimestamp.class, new JodaTimeTransformer());

        MongoCredential mongoCredential = MongoCredential.createCredential(
                PersistenceConfig.getDatabaseUsername(),
                PersistenceConfig.getDatabaseName(),
                PersistenceConfig.getDatabasePassword().toCharArray()
        );
        ServerAddress databaseAddress = new ServerAddress(PersistenceConfig.getDatabaseHost(),
                PersistenceConfig.getDatabasePortNumber());

        return new MongoClient(databaseAddress, Collections.singletonList(mongoCredential));
    }

    @Bean
    public MongoDatabase mongoDatabase() {
        return mongoClient().getDatabase(PersistenceConfig.getDatabaseName());
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), PersistenceConfig.getDatabaseName());
    }
}
