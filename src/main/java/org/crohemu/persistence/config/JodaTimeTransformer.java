package org.crohemu.persistence.config;

import org.bson.Transformer;
import org.bson.types.BSONTimestamp;
import org.joda.time.Instant;

/**
 * Transforms MongoDb's BSONTimestamp into JodaTime's Instant.
 */
public class JodaTimeTransformer implements Transformer {
    @Override
    public Object transform(Object o) {
        if (o instanceof BSONTimestamp) {
            return new Instant((long) ((BSONTimestamp) o).getTime() * 1000L);
        } else if (o instanceof Instant) {
            return new BSONTimestamp((int) (((Instant) o).getMillis() / 1000L), 0);
        }

        throw new IllegalArgumentException("JodaTimeTransformer can only be used with java.util.Date or joda's Instant.");
    }
}
