package org.crohemu.persistence.character;

import org.crohemu.model.character.Emoticon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface EmoticonRepository extends MongoRepository<Emoticon, String> {
    @Query(value = "{}", fields = "{ 'id' : 1 }")
    List<Emoticon> findAllIds0();
    default List<Byte> findAllIds() {
        return findAllIds0().stream().map(e -> (byte) e.getId()).collect(Collectors.toList());
    }
}
