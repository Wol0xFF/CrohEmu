package org.crohemu.persistence.world;

import org.crohemu.model.world.GameMap;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MapPositionRepository extends MongoRepository<GameMap, String> {
    GameMap findById(int id);
}
