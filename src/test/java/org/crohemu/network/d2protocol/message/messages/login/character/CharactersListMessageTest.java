package org.crohemu.network.d2protocol.message.messages.login.character;

import org.crohemu.network.tcpwrapper.TcpClient;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;

public class CharactersListMessageTest {
    @Test
    public void deserialize() throws Exception {
        byte[] messageData = DatatypeConverter.parseHexBinary("025d420001002da180d08dda0a00084972632d4372" +
                "6f681a01000651d410ab02bf01ce039b080005010000000200000003000000040000000500000000018c010000" +
                "080100");

        CharactersListMessage message = new CharactersListMessage(messageData, new TcpClient(null));
        System.out.println(message.toString());
    }

}