package org.crohemu.network.d2protocol.utils;

import org.junit.Assert;
import org.junit.Test;

public class BooleanFlagsWriterTest {
    @Test
    public void getFromBooleansTest() {
        byte result = BooleanFlagsWriter.flagsToByte(true, true, false, true, false);
        Assert.assertEquals(0b01011, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooManyFlagsTest() {
        BooleanFlagsWriter.flagsToByte(true, true, true, true, true, true, true, true, true);
    }
}